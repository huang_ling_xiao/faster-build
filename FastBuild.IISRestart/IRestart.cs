﻿namespace FastBuild.IISRestart
{
    public interface IRestart
    {
        Task Execute();
    }
}
