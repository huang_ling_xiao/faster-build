﻿namespace FastBuild.Init.IISRestart
{
    [Injection(DIPattern.Singleton)]
    internal class IISRestartService : IDisposable
    {
        private ILog _log;
        private IISRestartInitOption _option;

        public IISRestartService(ILog log)
        {
            _log = log;
        }

        public void Run(IISRestartInitOption option)
        {
            _option = option;
        }

        public void Dispose()
        {
#if !DEBUG
            if (!string.IsNullOrEmpty(_option.ThisApplicationIpHost) && !string.IsNullOrEmpty(_option.IISRestartTaskIpHost))
            {
                var url = $"{_option.IISRestartTaskIpHost}/IISRestart?callback={_option.ThisApplicationIpHost}/IISRestart";
                var httpclient = new HttpClient();
                var respone = httpclient.GetAsync(url).Result;
                respone.Dispose();
                httpclient.Dispose();
                _log.Info($"IISRestart already send restart application request", "IISRestart");
            }
#endif
        }
    }
}
