﻿namespace FastBuild.Init.IISRestart
{
    /// <summary>
    /// iis回收导致的服务暂停
    /// 目前采取的办法是再跑一套本地的程序，在服务被回收的时候调用另外一套程序，重复十次调用本程序的重启服务接口（IIS在回收后只需要重新调用服务资源就会重启服务）
    /// gitee中有Release版本，路径：IISRestart/IISRestart_Release，在appsettings.json中修改端口号，双击exe启动或者部署到windows服务，推荐使用windows服务
    /// 如果你没有部署在iis，请忽略
    /// </summary>
    public class IISRestartInitOption
    {
        /// <summary>
        /// IISRestart.exe服务的Ip和端口，例：http://localhost:5666
        /// </summary>
        public string IISRestartTaskIpHost { get; set; }

        /// <summary>
        /// 本程序服务的Ip和端口，例：http://localhost:5667
        /// </summary>
        public string ThisApplicationIpHost { get; set; }
    }
}
