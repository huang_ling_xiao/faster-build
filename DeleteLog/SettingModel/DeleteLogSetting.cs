﻿namespace DeleteLog.SettingModel
{
    /// <summary>
    /// 
    /// </summary>
    public class DeleteLogSetting
    {
        public List<DeleteLogItem> Items { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteLogItem
    {
        /// <summary>
        /// Logs的根目录
        /// </summary>
        public string BasePath { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
        public List<string> Tags { get; set; }

        /// <summary>
        /// 保留几天的日志 默认30天
        /// </summary>
        public int KeepDay { get; set; } = 10;
    }
}
