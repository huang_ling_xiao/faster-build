﻿namespace FastBuild
{
    /// <summary>
    /// DependencyInjection 注入模式
    /// </summary>
    public enum DIPattern
    {
        Transient = 1,
        Scoped = 2,
        Singleton = 3
    }

    /// <summary>
    /// 日志等级
    /// </summary>
    public enum Level
    {
        Debug = 1,
        Info = 2,
        Error = 3,
        Warning = 4
    }

    /// <summary>
    /// 结果枚举
    /// </summary>
    public enum JobStatus
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 失败
        /// </summary>
        Error = 2
    }

    public enum InitMethodType
    {
        ServiceCollection = 1,
        WebApplication = 2
    }

    public enum InitModuleType
    {
        IISRestart = 1,
        TimeJob = 2
    }
}
