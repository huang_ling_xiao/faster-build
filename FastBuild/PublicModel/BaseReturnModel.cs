﻿using Newtonsoft.Json;

namespace FastBuild
{
    /// <summary>
    /// 公共返回类
    /// </summary>
    public class BaseReturnModel
    {
        [JsonProperty(Order = 1)]
        public int Code { get; set; }

        [JsonProperty(Order = 3)]
        public object Data { get; set; }

        [JsonProperty(Order = 4)]
        public object Error { get; set; }

        [JsonProperty(Order = 2)]
        public long TimeSpan = new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds();
    }
}
