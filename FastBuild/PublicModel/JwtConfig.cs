﻿namespace FastBuild
{
    public class JwtConfig
    {
        /// <summary>
        /// 签名key
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 生命周期
        /// </summary>
        public int Lifetime { get; set; } = 1200;

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMessage { get; set; } = "Unauthorized";

        /// <summary>
        /// 是否开启全局认证
        /// </summary>
        public bool OpenGlobalAuthentication { get; set; } = false;

        /// <summary>
        /// 请求Header中Token的名字
        /// </summary>
        public string HeaderName { get; set; } = "Authorization";
    }
}
