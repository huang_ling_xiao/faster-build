﻿namespace FastBuild
{
    /// <summary>
    /// 401 UnauthorizedException
    /// </summary>
    public class UnauthorizedException : Exception
    {
        /// <summary>
        /// 401 UnauthorizedException
        /// </summary>
        public UnauthorizedException()
            : base() { }

        /// <summary>
        /// 401 UnauthorizedException
        /// </summary>
        /// <param name="message"></param>
        public UnauthorizedException(string message)
            : base(message) { }
    }
}
