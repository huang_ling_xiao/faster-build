﻿namespace FastBuild
{
    /// <summary>
    /// JsonSetting Init Error
    /// </summary>
    public class JsonSettingException : Exception
    {
        /// <summary>
        /// JsonSetting Init Error
        /// </summary>
        public JsonSettingException()
        {

        }

        /// <summary>
        /// JsonSetting Init Error
        /// </summary>
        /// <param name="message"></param>
        public JsonSettingException(string message) : base(message) { }

        /// <summary>
        /// JsonSetting Init Error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public JsonSettingException(string message, Exception ex) : base(message, ex) { }
    }
}
