﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    /// <summary>
    /// Inject Init Error
    /// </summary>
    public class UnableInjectionDependencyException : SystemException
    {
        /// <summary>
        /// Inject Init Error
        /// </summary>
        public UnableInjectionDependencyException()
        {

        }

        /// <summary>
        /// Inject Init Error
        /// </summary>
        /// <param name="message"></param>
        public UnableInjectionDependencyException(string message) : base(message) { }

    }
}
