﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    /// <summary>
    /// 不能还原服务异常
    /// </summary>
    public class UnableResolveDependencyException : SystemException
    {

        /// <summary>
        /// 不能还原服务异常
        /// </summary>
        public UnableResolveDependencyException()
        {

        }

        /// <summary>
        /// 不能还原服务异常
        /// </summary>
        /// <param name="message"></param>
        public UnableResolveDependencyException(string message) : base(message) { }

    }
}
