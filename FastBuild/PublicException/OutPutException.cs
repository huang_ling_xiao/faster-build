﻿namespace FastBuild
{
    /// <summary>
    /// 450 业务校验失败 不进行日志记录
    /// </summary>
    public class OutPutException : Exception
    {
        /// <summary>
        /// 450 业务校验失败
        /// </summary>
        public OutPutException()
            : base() { }

        /// <summary>
        /// 450 业务校验失败
        /// </summary>
        /// <param name="message"></param>
        public OutPutException(string message)
            : base(message) { }
    }
}
