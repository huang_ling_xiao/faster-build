﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace FastBuild.Init.Cors
{
    internal static class CrosInitClass
    {
        private static string GlobalCrosName = "FastBuildCros";

        /// <summary>
        /// ADD Cros
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static IServiceCollection CrosInit(this IServiceCollection services)
        {
            if (OptionSetting.CrosInitOption is null) return services;

            var option = OptionSetting.CrosInitOption;

            var settingPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, option.SettingPath);
            if (!File.Exists(settingPath))
                return services;

            var jsonString = File.ReadAllText(settingPath);

            var settings = JsonConvert.DeserializeObject<List<CroSetting>>(jsonString, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            if (settings.Count != 0)
                option.CrosSettings.AddRange(settings);

            foreach (var item in option.CrosSettings)
            {
                if(item.Origins.Count>1)
                    item.Origins.RemoveAt(0);
                if (item.Headers.Count > 1)
                    item.Headers.RemoveAt(0);
                if (item.Methods.Count > 1)
                    item.Methods.RemoveAt(0);
            }

            var globalCros = option.CrosSettings.Where(s => string.IsNullOrEmpty(s.Name)).ToList();

            if (globalCros.Count() > 1)
                throw new Exception("只能存在一个全局Cros");

            if (globalCros.Count == 1)
                globalCros[0].Name = GlobalCrosName;

            if (option.CrosSettings.Select(s => s.Name).Distinct().Count() != settings.Count)
                throw new Exception("CrosName不能重复");

            services.AddCors(cors =>
            {
                //policy
                //.SetIsOriginAllowed(s => true)
                //.AllowAnyMethod() // 允许所有请求方法
                //.AllowAnyHeader() // 允许所有请求头
                //.AllowCredentials(); // 允许Cookie信息

                foreach (var item in option.CrosSettings)
                {
                    cors.AddPolicy(item.Name, policy =>
                    {
                        if (string.Join("", item.Origins.Distinct()) == "*")
                            policy.SetIsOriginAllowed(s => true);
                        else
                            policy.WithOrigins(item.Origins.Distinct().ToArray());
                        policy
                        .WithMethods(item.Methods.Distinct().ToArray())
                        .WithHeaders(item.Headers.Distinct().ToArray());

                        if (item.ExposedHeaders.Count > 0)
                            policy.WithExposedHeaders(item.ExposedHeaders.ToArray());

                        if (item.AllowCredentials)
                            policy.AllowCredentials();
                    });
                }
            });

            return services;
        }

        /// <summary>
        /// Use Cros
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static WebApplication CrosInit(this WebApplication app)
        {
            if (OptionSetting.CrosInitOption is null) return app;

            if (OptionSetting.CrosInitOption.CrosSettings.Any(s => s.Name == GlobalCrosName))
                app.UseCors(GlobalCrosName);

            return app;
        }
    }
}
