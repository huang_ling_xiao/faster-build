﻿namespace FastBuild.Init.Cors
{
    /// <summary>
    /// Cros 配置
    /// </summary>
    public class CrosInitOption
    {
        /// <summary>
        /// 配置文件地址
        /// </summary>
        public string SettingPath { get; set; } = "Setting/Cros.json";

        /// <summary>
        /// 添加Cros
        /// </summary>
        /// <param name="action"></param>
        public void AddCros(Action<CroSetting> action)
        {
            //if (string.IsNullOrEmpty(name))
            //    throw new ArgumentNullException("AddCros name 不能为空");

            CroSetting setting = new CroSetting();

            action(setting);

            if (string.IsNullOrEmpty(setting.Name) && CrosSettings.Any(s => string.IsNullOrEmpty(s.Name)))
                throw new Exception("只能存在一个全局Cros");

            CrosSettings.Add(setting);
        }

        internal List<CroSetting> CrosSettings = new List<CroSetting>();
    }

    /// <summary>
    /// 配置
    /// </summary>
    public class CroSetting
    {
        /// <summary>
        /// Name 为空则为全局
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 允许的域名 默认允许全部
        /// </summary>
        public List<string> Origins { get; set; } = new List<string>() { "*" };

        /// <summary>
        /// 允许的头部（HTTP Headers）字段 默认允许全部
        /// </summary>
        public List<string> Headers { get; set; } = new List<string>() { "*" };

        /// <summary>
        /// 允许的 HTTP 请求方式 默认允许全部
        /// </summary>
        public List<string> Methods { get; set; } = new List<string>() { "*" };

        /// <summary>
        /// 允许在响应中暴露的头部（HTTP Headers）字段
        /// </summary>
        public List<string> ExposedHeaders { get; set; } = new List<string>() { };

        /// <summary>
        /// 允许携带
        /// </summary>
        public bool AllowCredentials { get; set; } = false;
    }
}
