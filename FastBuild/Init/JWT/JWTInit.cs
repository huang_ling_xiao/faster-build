﻿using Microsoft.Extensions.Configuration;

namespace FastBuild.Init.JWT
{
    internal static class JWTInitClass
    {
        /// <summary>
        /// 初始化JWT
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection JWTInit(this IServiceCollection services, IConfiguration configuration)
        {
            JwtConfig config = new JwtConfig();
            configuration.GetSection("Jwt").Bind(config);

            if (config.OpenGlobalAuthentication && string.IsNullOrEmpty(config.SecretKey))
                throw new Exception("请设置SecretKey密钥");

            services.AddSingleton(config);

            return services;
        }
    }
}
