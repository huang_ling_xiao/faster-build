﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Linq;
using System.Security.Claims;

namespace FastBuild.Init.JWT
{
    [Injection(DIPattern.Singleton)]
    internal class JwtHelper : IJwtHelper
    {
        private JwtConfig _jwtConfig;

        private byte[] _signingKey = null;

        private byte[] signingKey
        {
            get
            {
                if (_signingKey == null)
                    GenrateSigningKey();
                return _signingKey;
            }
            set { }
        }

        public JwtHelper(JwtConfig jwtConfig)
        {
            _jwtConfig = jwtConfig;
        }

        public bool DecryptOK(string token)
        {
            //为空
            if (string.IsNullOrEmpty(token)) return false;

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

            TokenValidationParameters validationParameters = new TokenValidationParameters
            {
                RequireExpirationTime = true,
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = new SymmetricSecurityKey(signingKey),//签名验证算法
                ValidateLifetime = true,//是否验证失效时间,
                ValidateIssuerSigningKey = true,
                ValidateAudience = false,
                ValidateIssuer = false,
            };

            try
            {
                handler.ValidateToken(token, validationParameters, out var securityToken);
            }
            catch (Exception ex)
            {
                return false;

                //Token过期 ToeknExpired
                if (ex.GetType().Name == typeof(SecurityTokenExpiredException).Name)
                    return false;
                //签名伪造 ToeknSignatureKeyNotFound
                else if (ex.GetType().Name == typeof(SecurityTokenSignatureKeyNotFoundException).Name)
                    return false;
            }
            return true;
        }

        public T Decrypt<T>(string token)
        {
            if (DecryptOK(token))
            {
                return GetTokenData<T>(token, out var lifetime);
            }
            throw new Exception("Token decrypt fail");
        }

        public string Encrypt(object body) => Encrypt(body, 0);

        public string Encrypt(object body, int second)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            //秘钥 (SymmetricSecurityKey 对安全性的要求，密钥的长度太短会报出异常)
            var key = new SymmetricSecurityKey(signingKey);
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtHeader header = new JwtHeader(creds);

            JwtPayload payload = new JwtPayload();

            var nowTime = DateTime.Now;

            //JObject特殊处理
            //TODO 还有JArry未处理
            if (body.GetType().Name == "JObject")
                body = JsonConvert.DeserializeObject<Dictionary<string, object>>(body.ToString());

            payload.Add("Data", body);
            payload.Add("exp", new DateTimeOffset(nowTime.AddSeconds(second == 0 ? _jwtConfig.Lifetime : second)).ToUnixTimeSeconds());
            payload.Add("iat", new DateTimeOffset(nowTime).ToUnixTimeSeconds());

            var jwt = new JwtSecurityToken(header, payload);

            return tokenHandler.WriteToken(jwt);
        }

        public string RefreshToken(string token, int lifetime = 0)
        {
            var tokenData = GetTokenData<object>(token, out var oldlifetime);
            if (lifetime == 0)
                lifetime = oldlifetime;

            return Encrypt(tokenData, lifetime);
        }

        /// <summary>
        /// 生成签名
        /// 需要用到byte[64]位固定长度签名,所以这里用签名算法做是最为稳妥的.
        /// </summary>
        private void GenrateSigningKey()
        {
            if (string.IsNullOrEmpty(_jwtConfig.SecretKey))
                throw new Exception("请在appsettings.json中添加Jwt配置后再使用IJwtHelper类");

            ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(_jwtConfig.SecretKey);
            byte[] messageBytes = encoding.GetBytes(_jwtConfig.SecretKey);
            using (HMACSHA256 hmacsha256 = new HMACSHA256(keyByte))
            {
                _signingKey = hmacsha256.ComputeHash(messageBytes);
            }
        }

        private T GetTokenData<T>(string token, out int lifetime)
        {
            lifetime = 0;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken decryptToken = handler.ReadJwtToken(token);

            #region exp
            var iattimex = decryptToken.Payload.FirstOrDefault(s => s.Key == "iat").Value?.ToString();
            var exptimex = decryptToken.Payload.FirstOrDefault(s => s.Key == "exp").Value?.ToString();

            if (!string.IsNullOrEmpty(iattimex) && !string.IsNullOrEmpty(exptimex))
            {
                DateTime inGMT = new DateTime(1970, 1, 1, 8, 0, 0, 0, DateTimeKind.Utc);
                var time = DateTimeOffset.FromUnixTimeSeconds(long.Parse(exptimex) - long.Parse(iattimex)).ToLocalTime().DateTime;
                lifetime = int.Parse((time - inGMT).TotalSeconds.ToString());
            }

            #endregion

            var dataObj = decryptToken.Payload.Single(s => s.Key == "Data").Value;

            var dataObjType = dataObj.GetType();

            if (dataObjType.Name == "JObject")
                dataObj = JObject.Parse(dataObj.ToString());
            else if (dataObjType.Name == "JArray")
                dataObj = JArray.Parse(dataObj.ToString());

            if (dataObjType.Name == typeof(T).Name)
                return (T)dataObj;
            else if (dataObjType.Name == "String")
                return (T)dataObj;
            else if (typeof(T).Name == "String")
                return (T)(object)JsonConvert.SerializeObject(dataObj);

            return JsonConvert.DeserializeObject<T>(dataObj.ToString());
        }
    }
}
