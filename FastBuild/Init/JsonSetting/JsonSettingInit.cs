﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace FastBuild.Init.Setting
{
    /// <summary>
    /// Setting初始化
    /// </summary>
    internal static class SettingInitClass
    {
        /// <summary>
        /// 添加json配置项
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection JsonSettingInit(this IServiceCollection services)
        {
            if (OptionSetting.SettingInitOption is null) return services;

            var settingInitNameList = new List<string>();

            var option = OptionSetting.SettingInitOption;

            var allSettingModel = AssemblyHelper.LoadTypeAssemblyByNamespace(option.SettingModelNamespace, option.AssemblyName, isInterface: false);

            var folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, option.SettingJsonFolderName);

            //不存在退出
            if (!Directory.Exists(folderPath)) return services;


            var allSettingJsonFile = Directory.GetFiles(folderPath, "*.json").ToList();

            //InitEnableCheck
            InitCommon.CheckIgnoreClass(allSettingModel);

            foreach (var settingModel in allSettingModel)
            {
                //特性json文件路径
                var jsonFromAttribute = settingModel.GetCustomAttribute<JsonFromAttribute>();
                if (jsonFromAttribute is not null)
                {
                    var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, jsonFromAttribute.Path);
                    if (File.Exists(filePath))
                    {
                        if (services.AddJsonDI(filePath, settingModel))
                            settingInitNameList.Add(settingModel.Name);
                        continue;
                    }
                }

                //直接匹配
                var oneFilePath = allSettingJsonFile.FirstOrDefault(path => path.EndsWith($"{settingModel.Name}.json"));

                if (string.IsNullOrEmpty(oneFilePath)) continue;

                if (services.AddJsonDI(oneFilePath, settingModel))
                    settingInitNameList.Add(settingModel.Name);
            }

            if (settingInitNameList.Count > 0)
                return services.ConsoleWriteLine($"JsonSetting Init Success", settingInitNameList);
            else
                return services;
        }

        public static bool AddJsonDI(this IServiceCollection services, string filePath, Type settingModel)
        {
            Dictionary<PropertyInfo, string> valueMaps = new Dictionary<PropertyInfo, string>();
            foreach (var property in settingModel.GetProperties())
            {
                var valueMap = property.GetCustomAttribute<ValueFromAttribute>();
                if (valueMap is not null && !string.IsNullOrEmpty(valueMap.Property))
                {
                    valueMaps.Add(property, valueMap.Property);
                }
            }

            var jsonInstance = JsonConvert.DeserializeObject(File.ReadAllText(filePath), settingModel);

            if (valueMaps.Count > 0)
            {
                if (jsonInstance is null)
                    jsonInstance = Activator.CreateInstance(settingModel);

                var jobject = JObject.Parse(File.ReadAllText(filePath));

                foreach (var valueMap in valueMaps)
                {
                    try
                    {
                        valueMap.Key.SetValue(jsonInstance, valueMap.Key.PropertyType.jsonStringToObject(jobject[valueMap.Value].ToString()));
                    }
                    catch (Exception ex)
                    {
                        throw new JsonSettingException($"{{{settingModel.FullName}.{valueMap.Key.Name}}} Property Set Value Error ", ex);
                    }
                }
            }

            if (jsonInstance is not null)
            {
                services.AddSingleton(settingModel, jsonInstance);
                return true;
            }
            return false;
        }
    }
}
