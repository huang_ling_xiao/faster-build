﻿namespace FastBuild.Init.Setting
{
    public class SettingInitOption
    {
        /// <summary>
        /// 程序集名称 默认为主程序集
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// json文件存放的文件夹名字（默认叫做Setting，根目录下）
        /// </summary>
        public string SettingJsonFolderName { get; set; } = "Setting";

        /// <summary>
        /// json文件对应的Model模型所在的命名空间 默认：项目名称.SettingModel
        /// {EntryProject}是指当前项目名称的标识
        /// </summary>
        public string SettingModelNamespace { get; set; } = "{EntryProject}.SettingModel";
    }
}
