﻿using FastBuild.Init;
using FastBuild.Init.Cors;
using FastBuild.Init.DI;
using FastBuild.Init.JWT;
using FastBuild.Init.MyLog;
using FastBuild.Init.Setting;
using FastBuild.MyNLog;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace FastBuild
{
    /// <summary>
    /// 
    /// </summary>
    public static class FastBuilder
    {
        /// <summary>
        /// Init入口
        /// </summary>
        /// <param name="args"></param>
        /// <param name="optionBuild"></param>
        /// <param name="webApplicationBuild"></param>
        public static void FastBuild(string[] args,
            Action<InitOption, IConfiguration> optionBuild,
            Action<WebApplicationBuilder> webApplicationBuild = null)
        {
            var builder = WebApplication.CreateBuilder(args);
            //getServices
            var services = builder.Services;
            try
            {
                webApplicationBuild?.Invoke(builder);

                builder
                    .FastBuild(optionBuild)
                    .Run();
            }
            catch (Exception ex)
            {
                if (!OptionSetting.UseLog)
                    builder.LogInit();

                GlobalLog.LogGlobalException(ex);
                //线程休眠30毫秒 不然日志写不进去
                Thread.Sleep(30);
                throw;
            }
        }

        /// <summary>
        /// Init入口
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="optionBuild"></param>
        public static WebApplication FastBuild(this WebApplicationBuilder builder,
            Action<InitOption, IConfiguration> optionBuild)
        {
            //getServices
            var services = builder.Services;

            //builderInit
            return builder
                .LogInit()
                .InitOption(optionBuild)
                .ServiceBuild(ConfigureSetting.ServiceAction)
                .ApplicationBuild(ConfigureSetting.ApplicationAction);
        }
    }

    /// <summary>
    /// 拓展方法
    /// </summary>
    internal static class WebApplicationBuilderExtensions
    {
        public static WebApplicationBuilder InitOption(
            this WebApplicationBuilder builder,
            Action<InitOption, IConfiguration> optionBuild)
        {
            //optionBuild
            optionBuild?.Invoke(new InitOption(), builder.Configuration);

            builder.SaveInitOption();

            return builder;
        }

        public static WebApplicationBuilder ServiceBuild(this WebApplicationBuilder builder, Action<IServiceCollection> serviceAction)
        {
            builder.Services
                //注入Http类 IHttpContextAccessor
                .AddSingleton(typeof(IHttpContextAccessor), typeof(HttpContextAccessor))
                .JsonSettingInit()
                .DIInit()
                .InvokeInitMethoad(InitModuleType.IISRestart, InitMethodType.ServiceCollection)
                .InvokeInitMethoad(InitModuleType.TimeJob, InitMethodType.ServiceCollection)
                .JWTInit(builder.Configuration)
                .SwaggerInit(builder.Configuration)
                .CrosInit()
                .WebApplicationBaseInit(serviceAction);

            return builder;
        }

        public static WebApplication ApplicationBuild(this WebApplicationBuilder builder, Action<WebApplication> applicationAction)
        {
            var application = builder.Build()
                .SwaggerInit(builder.Configuration)
                .CrosInit()
                .WebApplicationBaseInit(applicationAction)
                .InvokeInitMethoad(InitModuleType.IISRestart, InitMethodType.WebApplication)
                .InvokeInitMethoad(InitModuleType.TimeJob, InitMethodType.WebApplication);

            ServiceContainer.serviceProvider = application.Services;

            return application;
        }

        public static IServiceCollection InvokeInitMethoad(this IServiceCollection serviceCollection, InitModuleType moduleType, InitMethodType type, object[] parm = null)
        {
            if (parm != null)
                parm = new object[] { serviceCollection }.Concat(parm).ToArray();
            else
                parm = new object[] { serviceCollection };

            InvokeInitMethoad(moduleType, type, parm);

            return serviceCollection;
        }

        public static WebApplication InvokeInitMethoad(this WebApplication webApplication, InitModuleType moduleType, InitMethodType type, object[] parm = null)
        {
            if (parm != null)
                parm = new object[] { webApplication }.Concat(parm).ToArray();
            else
                parm = new object[] { webApplication };

            InvokeInitMethoad(moduleType, type, parm);

            return webApplication;
        }

        public static void InvokeInitMethoad(InitModuleType moduleType, InitMethodType type, object[] parm)
        {
            var firstName = "FastBuild";
            var xxx = AppDomain.CurrentDomain.GetAssemblies().Where(s => s.FullName.Contains("IIS"));
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(s => s.FullName.StartsWith($"{firstName}.{moduleType},"));
            if (assembly == null) return;

            var initClass = assembly.GetType($"{firstName}.Init.{moduleType}.InitClass");
            if (initClass == null) return;

            var methoad = initClass.GetMethod($"InitMethod_{type}");
            if (methoad is not null)
                methoad.Invoke(null, parm);

            return;
        }
    }
}
