﻿using FastBuild.Init;
using FastBuild.Init.Cors;
using FastBuild.Init.DI;
using FastBuild.Init.Setting;
using FastBuild.Init.Swagger;
using Microsoft.AspNetCore.Mvc;

namespace FastBuild
{
    /// <summary>
    /// InitOption
    /// </summary>
    public class InitOption
    {
        /// <summary>
        /// 日志唯一值 传入Token，返回一个唯一key值
        /// </summary>
        public Func<string, string> LogUniqueKey
        {
            get { return OptionSetting.LogUniqueKey; }
            set { OptionSetting.LogUniqueKey = value; }
        }

        ///// <summary>
        ///// 使用Swagger 默认true
        ///// </summary>
        //public bool UseSwagger
        //{
        //    get { return OptionSetting.UseSwagger; }
        //    set { OptionSetting.UseSwagger = value; }
        //}

        /// <summary>
        /// UseSwagger
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public InitOption UseSwagger(Action<SwaggerInitOption> action)
        {
            OptionAction.SwaggerInitAction = action; return this;
        }

        /// <summary>
        /// 设置MVC的option
        /// </summary>
        /// <param name="mvcOption">构建Mvc的可选参数</param>
        /// <param name="mvcBuilder">Mvc构建完成以后的builder</param>
        /// <returns></returns>
        public InitOption SetMvcOption(Action<MvcOptions> mvcOption, Action<IMvcBuilder> mvcBuilder = null)
        {
            ConfigureSetting.MvcOptionAction = mvcOption;
            ConfigureSetting.MvcBuilderAction = mvcBuilder;
            return this;
        }

        /// <summary>
        /// IServiceCollection的拓展方法
        /// </summary>
        /// <param name="action"></param>
        public InitOption ConfigureServices(Action<IServiceCollection> action)
        {
            ConfigureSetting.ServiceAction = action; return this;
        }

        /// <summary>
        /// ApplicationBuilder的拓展方法
        /// </summary>
        /// <param name="action"></param>
        public InitOption Configure(Action<WebApplication> action)
        {
            ConfigureSetting.ApplicationAction = action; return this;
        }

        /// <summary>
        /// 启用Json配置
        /// </summary>
        /// <param name="action"></param>
        public InitOption UseJsonSetting(Action<SettingInitOption> action)
        {
            OptionAction.SettingInitAction = action; return this;
        }

        /// <summary>
        /// 启用Json配置
        /// </summary>
        public InitOption UseJsonSetting()
        {
            OptionAction.SettingInitAction = (a) => new SettingInitOption(); return this;
        }

        /// <summary>
        /// 使用自定义注入
        /// </summary>
        /// <param name="action"></param>
        public InitOption UseInjection(Action<DIInitOption> action)
        {
            OptionAction.DIInitAction = action; return this;
        }

        ///// <summary>
        ///// 启用IISRestart
        ///// </summary>
        ///// <param name="action"></param>
        //public InitOption UseIISRestart(Action<IISRestartInitOption> action)
        //{
        //    OptionAction.IISRestartInitAction = action; return this;
        //}

        ///// <summary>
        ///// 启用TimeJob
        ///// </summary>
        ///// <param name="action"></param>
        //public InitOption UseTimeJob(Action<TimeJobInitOption> action)
        //{
        //    OptionAction.TimeJobInitAction = action; return this;
        //}

        ///// <summary>
        ///// 启用TimeJob
        ///// </summary>
        //public InitOption UseTimeJob()
        //{
        //    OptionAction.TimeJobInitAction = (a) => new TimeJobInitOption(); return this;
        //}

        ///// <summary>
        ///// 请求日志
        ///// </summary>
        //public InitOption UseGlobalRequestLog(Action<RequestLogOption> action)
        //{
        //    OptionAction.RequestLogAction = action; return this;
        //}

        /// <summary>
        /// 请求日志
        /// </summary>
        public InitOption UseGlobalRequestLog()
        {
            OptionAction.RequestLogAction = (a) => new RequestLogOption(); return this;
        }

        /// <summary>
        /// 使用Cros
        /// </summary>
        public InitOption UseCros(Action<CrosInitOption> action)
        {
            OptionAction.CrosInitAction = action; return this;
        }

        /// <summary>
        /// 使用Cros
        /// </summary>
        public InitOption UseCros()
        {
            OptionAction.CrosInitAction = (a) => new CrosInitOption(); return this;
        }
    }
}
