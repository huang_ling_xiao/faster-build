﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;

namespace FastBuild.Init
{
    internal static class InitBase
    {
        public static IServiceCollection WebApplicationBaseInit(this IServiceCollection services, Action<IServiceCollection> serviceAction)
        {

            #region MVC相关
            var mvcBuilder = services.AddControllers(option =>
                 {
                     option.Filters.Add<AutowiredFilter>();

                     ConfigureSetting.MvcOptionAction?.Invoke(option);
                 })
                .AddJsonOptions(option =>
                {
                    option.JsonSerializerOptions.PropertyNamingPolicy = null;
                });

            ConfigureSetting.MvcBuilderAction?.Invoke(mvcBuilder);
            #endregion

            //取消请求实体验证
            services.Configure<ApiBehaviorOptions>((o) =>
            {
                o.SuppressModelStateInvalidFilter = true;
            });

            //执行action
            serviceAction?.Invoke(services);

            return services;
        }

        public static WebApplication WebApplicationBaseInit(this WebApplication app, Action<WebApplication> applicationAction)
        {
            //Mvc
            app.UseRouting();
            app.UseMiddleware<RequestLogMiddleware>().ConsoleWriteLine("RequestLogMiddleware Init Success");
            app.UseMiddleware<GlobalRequestMiddleware>().ConsoleWriteLine("GlobalRequestMiddleware Init Success");
            app.UseMiddleware<JwtMiddleware>().ConsoleWriteLine("JwtMiddleware Init Success");
            app.UseEndpoints(u => { u.MapControllers(); }).ConsoleWriteLine("Mvc Init Success");

            //UseStaticFiles
            app.UseStaticFiles().ConsoleWriteLine("StaticFiles Init Success");

            //执行action
            applicationAction?.Invoke(app);

            return app;
        }
    }
}
