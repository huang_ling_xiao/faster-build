﻿using FastBuild.Init.Cors;
using FastBuild.Init.DI;
//using FastBuild.Init.IISRestart;
using FastBuild.Init.Setting;
using FastBuild.Init.Swagger;

//using FastBuild.Init.TimeJob;
using Microsoft.AspNetCore.Mvc;

namespace FastBuild.Init
{
    /// <summary>
    /// 配置Action
    /// </summary>
    internal static class OptionAction
    {
        /// <summary>
        /// SettingInitAction
        /// </summary>
        public static Action<SettingInitOption> SettingInitAction { get; set; }

        /// <summary>
        /// DIInitAction
        /// </summary>
        public static Action<DIInitOption> DIInitAction { get; set; }

        ///// <summary>
        ///// IISRestartInitAction
        ///// </summary>
        //public static Action<IISRestartInitOption> IISRestartInitAction { get; set; }

        ///// <summary>
        ///// TimeJobInitAction
        ///// </summary>
        //public static Action<TimeJobInitOption> TimeJobInitAction { get; set; }

        /// <summary>
        /// RequestLogAction
        /// </summary>
        public static Action<RequestLogOption> RequestLogAction { get; set; }

        /// <summary>
        /// CrosInitAction
        /// </summary>
        public static Action<CrosInitOption> CrosInitAction { get; set; }

        /// <summary>
        /// SwaggerInitAction
        /// </summary>
        public static Action<SwaggerInitOption> SwaggerInitAction { get; set; }
    }

    /// <summary>
    /// 配置setting
    /// </summary>
    internal static class OptionSetting
    {
        /// <summary>
        /// UseLog
        /// </summary>
        public static bool UseLog { get; set; } = false;

        ///// <summary>
        ///// Swagger
        ///// </summary>
        //public static bool UseSwagger { get; set; } = true;

        /// <summary>
        /// 传入Token，返回一个唯一key值
        /// </summary>
        public static Func<string, string> LogUniqueKey { get; set; }

        ///// <summary>
        ///// IISRestart的Option
        ///// </summary>
        //public static IISRestartInitOption IISRestartInitOption { get; set; }

        ///// <summary>
        ///// TimeJob的Option
        ///// </summary>
        //public static TimeJobInitOption TimeJobInitOption { get; set; }

        /// <summary>
        /// DIInit的Option
        /// </summary>
        public static DIInitOption DIInitOption { get; set; }

        /// <summary>
        /// SettingInit的Option
        /// </summary>
        public static SettingInitOption SettingInitOption { get; set; }

        /// <summary>
        /// RequestLog的Option
        /// </summary>
        public static RequestLogOption RequestLogOption { get; set; }

        /// <summary>
        /// CrosInit的Option
        /// </summary>
        public static CrosInitOption CrosInitOption { get; set; }

        /// <summary>
        /// MvcBuilder
        /// </summary>
        public static IMvcBuilder IMvcBuilder { get; set; }

        /// <summary>
        /// SwaggerInitOption
        /// </summary>
        public static SwaggerInitOption SwaggerInitOption { get; set; }
    }

    /// <summary>
    /// 系统级别Setting
    /// </summary>
    internal static class ConfigureSetting
    {
        /// <summary>
        /// IServiceCollection的拓展方法
        /// </summary>
        public static Action<IServiceCollection> ServiceAction { get; set; }

        /// <summary>
        /// ApplicationAction的拓展方法
        /// </summary>
        public static Action<WebApplication> ApplicationAction { get; set; }

        /// <summary>
        /// MvcOptions的拓展方法
        /// </summary>
        public static Action<MvcOptions> MvcOptionAction { get; set; }

        /// <summary>
        /// MvcBuilder的拓展方法
        /// </summary>
        public static Action<IMvcBuilder> MvcBuilderAction { get; set; }
    }
}
