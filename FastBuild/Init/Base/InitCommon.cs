﻿using System.Reflection;

namespace FastBuild.Init
{
    internal static class InitCommon
    {
        #region 注释掉
        ///// <summary>
        ///// 把自定义的Init配置添加到InitOption静态类中（后面可能用到 所有Init的Option项都需存储一下）
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="action"></param>
        ///// <param name="optionName"></param>
        ///// <returns></returns>
        //public static T InitOptionAdd<T>(Action<T> action, string optionName) where T : class, new()
        //{

        //    var initOptionType = typeof(InitSetting);

        //    T option = new T();

        //    action?.Invoke(option);

        //    initOptionType.GetProperty(optionName)?.SetValue(initOptionType, option, null);
        //    return option;
        //} 
        #endregion

        /// <summary>
        /// 初始化Option
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static WebApplicationBuilder SaveInitOption(this WebApplicationBuilder builder)
        {
            var actionType = typeof(OptionAction);

            var settingType = typeof(OptionSetting);

            var settingsAllProperties = settingType.GetProperties();

            foreach (var property in actionType.GetProperties())
            {
                if (property.GetValue(actionType) is null) continue;

                //获取Invoke方法
                var invokeMethod = property.PropertyType.GetMethod("Invoke");

                var parameter = invokeMethod.GetParameters()[0];

                //去InitSetting中寻找要返回的属性
                var oneSetting = settingsAllProperties.Where(s => s.PropertyType.Name == parameter.ParameterType.Name).FirstOrDefault();
                if (oneSetting is null) continue;

                //创建实例参数
                var instance = Activator.CreateInstance(parameter.ParameterType);
                //获取action实例
                object actionObject = property.GetValue(null);

                if (actionObject is not null)
                {
                    //执行action
                    invokeMethod.Invoke(actionObject, new object[] { instance });
                }

                //给InitSetting中的属性设置值
                oneSetting?.SetValue(settingType, instance, null);
            }
            return builder;
        }

        /// <summary>
        /// 校验InitEnable实现特性功能
        /// </summary>
        /// <param name="typeList"></param>
        public static void CheckIgnoreClass(List<Type> typeList)
        {
            var list = typeList
                .Select(s => new { IgnoreAttribute = s.GetCustomAttribute<InjectionIgnoreAttribute>(), OldType = s })
                .Where(s => s.IgnoreAttribute is not null)
                .Select(s => s.OldType)
                .ToList();
            typeList.RemoveAll(setting => list.Contains(setting));
        }

        /// <summary>
        /// 校验InitEnable实现特性功能
        /// </summary>
        /// <param name="typeList"></param>
        public static void CheckIgnoreClass(List<AddDIModel> typeList)
        {
            var list = typeList
                .Select(s => new { IgnoreAttribute = s.ServiceType.GetCustomAttribute<InjectionIgnoreAttribute>(), OldType = s })
                .Where(s => s.IgnoreAttribute is not null)
                .Select(s => s.OldType)
                .ToList();
            typeList.RemoveAll(setting => list.Contains(setting));
        }

        /// <summary>
        /// 添加DI公共方法
        /// </summary>
        /// <param name="services"></param>
        /// <param name="addDIModels"></param>
        /// <returns></returns>
        public static List<string> AddDICommon(this IServiceCollection services, List<AddDIModel> addDIModels)
        {
            var diInitNameList = new List<string>();

            //CheckInitEnable
            CheckIgnoreClass(addDIModels);

            addDIModels = addDIModels.OrderByDescending(s => s.DIPattern).ToList();

            foreach (var item in addDIModels)
            {
                if (item.IServiceType == null)
                {
                    services.AddDI(item.ServiceType, item.DIPattern);
                    if (item.ServiceType.IsPublic) diInitNameList.Add($"{item.ServiceType.Name}（{item.DIPattern}）");
                }
                else
                {
                    services.AddDI(item.IServiceType, item.ServiceType, item.DIPattern);
                    if (item.IServiceType.IsPublic) diInitNameList.Add($"{item.IServiceType.Name}（{item.DIPattern}）");
                }
            }
            return diInitNameList;
        }

        /// <summary>
        /// 添加DI
        /// </summary>
        /// <param name="services"></param>
        /// <param name="type"></param>
        /// <param name="dIPattern"></param>
        /// <returns></returns>
        public static IServiceCollection AddDI(this IServiceCollection services, Type type, DIPattern dIPattern)
        {
            switch (dIPattern)
            {
                case DIPattern.Transient:
                    services.AddTransient(type);
                    break;
                case DIPattern.Scoped:
                    services.AddScoped(type);
                    break;
                case DIPattern.Singleton:
                    services.AddSingleton(type);
                    break;
                default: break;
            }
            return services;
        }

        /// <summary>
        /// 添加DI
        /// </summary>
        /// <param name="services"></param>
        /// <param name="serviceType"></param>
        /// <param name="implementationType"></param>
        /// <param name="dIPattern"></param>
        /// <returns></returns>
        public static IServiceCollection AddDI(this IServiceCollection services, Type serviceType, Type implementationType, DIPattern dIPattern)
        {
            switch (dIPattern)
            {
                case DIPattern.Transient:
                    services.AddTransient(serviceType, implementationType);
                    break;
                case DIPattern.Scoped:
                    services.AddScoped(serviceType, implementationType);
                    break;
                case DIPattern.Singleton:
                    services.AddSingleton(serviceType, implementationType);
                    break;
                default: break;
            }
            return services;
        }
    }
}
