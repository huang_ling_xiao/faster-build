﻿namespace FastBuild.Init
{
    internal class AddDIModel
    {
        public Type IServiceType { get; set; }

        public Type ServiceType { get; set; }

        public DIPattern DIPattern { get; set; }
    }
}
