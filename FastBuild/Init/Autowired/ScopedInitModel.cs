﻿namespace FastBuild
{
    [Injection(DIPattern.Scoped)]
    internal class ScopedInitModel
    {
        public List<string> FullName { get; set; }=new List<string>();
    }
}
