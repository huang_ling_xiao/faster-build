﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    internal static class MemberInfoExtentsion
    {
        private static List<string> SingletonInitModel { get; set; } = new List<string>();

        internal static void SetValue(this MemberInfo memberInfo, object obj, object value, IServiceProvider serviceProvider)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Field:
                    ((FieldInfo)memberInfo).SetValue(obj, value);
                    break;
                case MemberTypes.Property:
                    ((PropertyInfo)memberInfo).SetValue(obj, value);
                    break;
                default:
                    throw new UnableResolveDependencyException($"Unable to resolve dependency {memberInfo.ReflectedType.FullName}");
            }

            //执行Init方法
            InvokeInitMethod(value, serviceProvider);
        }

        internal static Type GetRealType(this MemberInfo memberInfo)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)memberInfo).FieldType;
                case MemberTypes.Property:
                    return ((PropertyInfo)memberInfo).PropertyType;
                default:
                    throw new UnableResolveDependencyException($"Unable to resolve dependency {memberInfo.ReflectedType.FullName}");
            }
        }

        internal static void InvokeInitMethod(object value, IServiceProvider serviceProvider)
        {
            var instanceType = value.GetType();
            var injectionAttribute = instanceType.GetCustomAttribute<InjectionAttribute>();
            if (injectionAttribute != null && !string.IsNullOrEmpty(injectionAttribute.InitMethod))
            {
                ScopedInitModel scopedInitModel = null;
                if (injectionAttribute.DIPattern == DIPattern.Scoped)
                    scopedInitModel = serviceProvider.GetService<ScopedInitModel>();

                //单例和作用域模式下执行过一次InitMethod就直接return
                if (SingletonInitModel.Any(s => s == instanceType.FullName) || scopedInitModel?.FullName?.Any(s => s == instanceType.FullName) == true)
                    return;

                //单例 或者 作用域 添加对应的数据
                switch (injectionAttribute.DIPattern)
                {
                    case DIPattern.Scoped:
                        scopedInitModel.FullName.Add(instanceType.FullName);
                        break;
                    case DIPattern.Singleton:
                        SingletonInitModel.Add(instanceType.FullName);
                        break;
                }

                var initMethod = instanceType.GetMethod(injectionAttribute.InitMethod, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (initMethod != null)
                    initMethod.Invoke(value, null);
            }
        }
    }
}
