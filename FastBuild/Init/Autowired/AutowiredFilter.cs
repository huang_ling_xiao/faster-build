﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    internal class AutowiredFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            foreach (var filter in context.Filters)
            {
                filter.WithAutowired(context.HttpContext.RequestServices);
            }

            context.Controller.WithAutowired(context.HttpContext.RequestServices);
        }
    }
}
