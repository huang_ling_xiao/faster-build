﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    /// <summary>
    /// 属性注入帮助类
    /// </summary>
    internal static class DependencyInjection
    {
        private readonly static Type autowiredAttributeType = typeof(AutowiredAttribute);

        /// <summary>
        /// 属性依赖注入
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceProvider"></param>
        /// <param name="typeInstance"></param>
        public static void Resolve<T>(IServiceProvider serviceProvider, T typeInstance)
        {
            ResolveDependencyTree(serviceProvider, new InstanceScopeModel { Instance = typeInstance });
        }

        /// <summary>
        /// 分析
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="instanceScopeModel"></param>
        private static void ResolveDependencyTree(IServiceProvider serviceProvider, InstanceScopeModel instanceScopeModel)
        {
            foreach (var memberInfo in instanceScopeModel.Instance.GetType().GetFullMembers())
            {
                var customeAttribute = memberInfo.GetCustomAttribute(autowiredAttributeType, false);
                var memberType = ((AutowiredAttribute)customeAttribute).RealType ?? memberInfo.GetRealType();

                if (memberType == typeof(IServiceProvider))
                {
                    memberInfo.SetValue(instanceScopeModel.Instance, serviceProvider, serviceProvider);
                    continue;
                }

                var instance = GetInstance(instanceScopeModel, memberType);
                //如果依赖树能找到,则说明此处含有循环依赖,从依赖树还原
                //从parent instance 还原
                if (instance != null)
                {
                    memberInfo.SetValue(instanceScopeModel.Instance, instance, serviceProvider);
                    continue;
                }

                bool memberIsEnumerable = typeof(IEnumerable).IsAssignableFrom(memberType) && memberType.IsGenericType;
                if (memberIsEnumerable)
                {
                    Type elementType = memberType.GetGenericArguments()[0];
                    instance = serviceProvider.GetServices(elementType);
                }
                else
                {
                    instance = serviceProvider.GetServices(memberType)?.FirstOrDefault();
                }

                if (instance == null)
                {
                    throw new UnableResolveDependencyException($"Unable to resolve dependency {memberType.FullName}");
                }

                foreach (var instanceElement in (IEnumerable)(memberIsEnumerable ? instance : new object[] { instance }))
                {
                    //构建下一个节点
                    var nextInstanceScopeModel = new InstanceScopeModel
                    {
                        Instance = instanceElement,
                        ParentInstanceScope = instanceScopeModel
                    };
                    //递归注入的属性是否有其它依赖
                    ResolveDependencyTree(serviceProvider, nextInstanceScopeModel);
                }

                //将Instance赋值给属性
                memberInfo.SetValue(instanceScopeModel.Instance, instance, serviceProvider);
            }

            //将Instance赋值给属性
            MemberInfoExtentsion.InvokeInitMethod(instanceScopeModel.Instance, serviceProvider);
        }

        /// <summary>
        /// 递归查找父节点
        /// </summary>
        /// <param name="instanceScopeModel"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static object GetInstance(InstanceScopeModel instanceScopeModel, Type type)
        {
            if (type.IsInterface && type.IsAssignableFrom(instanceScopeModel.Instance.GetType()) ||
                instanceScopeModel.Instance.GetType() == type)
            {
                return instanceScopeModel.Instance;
            }
            if (instanceScopeModel.ParentInstanceScope == null) { return null; }
            return GetInstance(instanceScopeModel.ParentInstanceScope, type);
        }
    }
    internal class InstanceScopeModel
    {

        /// <summary>
        /// 实例
        /// </summary>
        public object Instance { get; set; }

        /// <summary>
        /// 父级
        /// </summary>
        public InstanceScopeModel ParentInstanceScope { get; set; }
    }

    internal static class AutowiredTypeExtension
    {
        private readonly static Type baseType = typeof(object);
        private readonly static Type autowiredAttributeType = typeof(AutowiredAttribute);

        public static IList<MemberInfo> GetFullMembers(this Type type)
        {
            return GetFullMembers(type, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly, new List<MemberInfo>());
        }

        private static IList<MemberInfo> GetFullMembers(this Type type, BindingFlags bindingFlags, List<MemberInfo> memberInfos)
        {
            memberInfos.AddRange(type.GetMembers(bindingFlags).Where(memberInfo => (memberInfo.MemberType == MemberTypes.Field || memberInfo.MemberType == MemberTypes.Property) && memberInfo.GetCustomAttribute(autowiredAttributeType, false) != null));
            if (type.BaseType == baseType)
            {
                return memberInfos;
            }
            return GetFullMembers(type.BaseType, bindingFlags, memberInfos);
        }
    }
}
