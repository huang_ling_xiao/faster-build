﻿using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace FastBuild.Init.MyLog
{
    internal static class LogInitClass
    {
        public static WebApplicationBuilder LogInit(this WebApplicationBuilder builder)
        {
            LogManager.ThrowConfigExceptions=true;
            LogManager.ThrowExceptions=true;
            LogManager.Setup().SetupExtensions(s => s.RegisterAssembly("NLog.Web.AspNetCore"));

            var config = new LoggingConfiguration();

            config.Variables.Add("logDirectory", Layout.FromString(
              @"${basedir}\Logs"));

            // Targets where to log to: File and Console
            #region Debugtarget
            config.Variables.Add("Debuglayout", Layout.FromString(
                  @"${longdate} | ${level} | ${event-properties:item=controllerName}${event-properties:item=actionName}${event-properties:item=uniqueKey}${newline}Message：${message}${newline}"));

            var debugTarget = new AsyncTargetWrapper(new FileTarget("Debugtarget")
            {
                FileName = config.Variables["logDirectory"] + @"\${level}\${shortdate}\${event-properties:item=fileName}.log",
                Layout = config.Variables["Debuglayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncDebugtarget", debugTarget);
            #endregion

            #region Infotarget
            config.Variables.Add("Infolayout", Layout.FromString(
                  @"${longdate} | ${level} | ${event-properties:item=controllerName}${event-properties:item=actionName}${event-properties:item=uniqueKey}${newline}Message：${message}${newline}"));

            var infoTarget = new AsyncTargetWrapper(new FileTarget("Infotarget")
            {
                FileName = config.Variables["logDirectory"] + @"\${level}\${shortdate}\${event-properties:item=fileName}.log",
                Layout = config.Variables["Infolayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncInfotarget", infoTarget);
            #endregion

            #region Errortarget
            config.Variables.Add("Errorlayout", Layout.FromString(
                  @"${longdate} | ${level} | ${event-properties:item=controllerName}${event-properties:item=actionName}${event-properties:item=uniqueKey}${newline}Message：${message}${newline}Exception：${exception:format=tostring}${newline}RequestParameter：${event-properties:item=requestParameter}${newline}Authorization：${event-properties:item=authorization}${newline}"));

            var errorTarget = new AsyncTargetWrapper(new FileTarget("Errortarget")
            {
                FileName = config.Variables["logDirectory"] + @"\${level}\${shortdate}\${event-properties:item=fileName}.log",
                Layout = config.Variables["Errorlayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncErrortarget", errorTarget);
            #endregion

            #region Sqltarget
            config.Variables.Add("Sqllayout", Layout.FromString(
                  @"${longdate} | Sql | ${event-properties:item=controllerName}${event-properties:item=actionName}${event-properties:item=uniqueKey}${newline}${message}${newline}"));

            var sqlTarget = new AsyncTargetWrapper(new FileTarget("Sqltarget")
            {
                FileName = config.Variables["logDirectory"] + @"\Sql\${shortdate}\${event-properties:item=fileName}.log",
                Layout = config.Variables["Sqllayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncSqltarget", sqlTarget);
            #endregion

            #region Requesttarget
            config.Variables.Add("Requestlayout", Layout.FromString(
              @"${longdate}  Url：${event-properties:item=url}  StatusCode：${event-properties:item=statuscode}  ExecuteTime：${event-properties:item=executeTime}${newline}${event-properties:item=uniqueKey}${event-properties:item=requestParameter}${event-properties:item=authorization}"));

            var requestTarget = new AsyncTargetWrapper(new FileTarget("Requesttarget")
            {
                FileName = config.Variables["logDirectory"] + @"\Request\${shortdate}\${event-properties:item=fileName}.log",
                Layout = config.Variables["Requestlayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncRequesttarget", requestTarget);
            #endregion

            #region GlobalExceptiontarget
            config.Variables.Add("GlobalExceptionlayout", Layout.FromString(
              @"${longdate}${newline}${event-properties:item=globalException}${newline}"));

            var globalExceptionTarget = new AsyncTargetWrapper(new FileTarget("GlobalExceptiontarget")
            {
                FileName = config.Variables["logDirectory"] + @"\Error\${shortdate}\GlobalException.log",
                Layout = config.Variables["GlobalExceptionlayout"]
            }, 1000, AsyncTargetWrapperOverflowAction.Grow);

            config.AddTarget("AsyncGlobalExceptiontarget", globalExceptionTarget);
            #endregion

            // Apply config
            LogManager.Configuration = config;

            OptionSetting.UseLog = true;

            return builder.ConsoleWriteLine($"Log Init Success");
        }
    }
}
