﻿using Microsoft.AspNetCore.Http;
using NLog;
using NLog.Common;
using NLog.Targets.Wrappers;
using Microsoft.AspNetCore.Mvc.Controllers;
using FastBuild.Init;
using FastBuild.Init.JWT;

namespace FastBuild.MyNLog
{
    [Injection(DIPattern.Singleton)]
    internal class RequestLog
    {
        private IHttpContextAccessor _httpContextAccessor { get; set; }

        public RequestLog(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public void LogRequest(double totalMilliseconds)
        {
            if (OptionSetting.RequestLogOption is null)
                return;

            var request = _httpContextAccessor.HttpContext.Request;

            var actionDescriptor = _httpContextAccessor.HttpContext.GetEndpoint()?.Metadata?.GetMetadata<ControllerActionDescriptor>();

            //Requesttarget
            LogEventInfo theEvent = new LogEventInfo(NLog.LogLevel.Info, "", "");

            theEvent.Properties["fileName"] = $"{actionDescriptor?.ControllerName}_{actionDescriptor?.ActionName}_{request.Method}";

            theEvent.Properties["url"] = request.PathBase + request.Path;

            theEvent.Properties["executeTime"] = totalMilliseconds.ToString("#0.000") + " s";

            theEvent.Properties["statuscode"] = _httpContextAccessor.HttpContext?.Response?.StatusCode + "";

            if (request is not null)
            {
                if (_httpContextAccessor.HttpContext?.Items?.ContainsKey("RequestToken") == true)
                {
                    theEvent.Properties["authorization"] = $"Authorization：{_httpContextAccessor.HttpContext?.Items["RequestToken"]}\n";

                    var tokenDecrypt = _httpContextAccessor.HttpContext?.Items["RequestTokenString"].ToString();

                    if (OptionSetting.LogUniqueKey is not null)
                    {
                        try
                        {
                            theEvent.Properties["uniqueKey"] = $"UniqueKey：{OptionSetting.LogUniqueKey.Invoke(tokenDecrypt)}\n";
                        }
                        catch { }
                    }
                }

                StringBuilder requestParameter = new StringBuilder();

                if (request.QueryString.HasValue)
                    requestParameter.Append($"Query:{request.QueryString.Value}\n");
                try
                {
                    if (request.Form is not null && request.Form.Count > 0)
                        requestParameter.Append($"Form:\n{string.Join("\n", request.Form.Select(form => $"Key:{form.Key},Value:{form.Value}").ToList())}\n");
                }
                catch
                {
                }

                try
                {
                    if (!actionDescriptor.ActionName.ToLower().StartsWith("upload") && request.Body.Length > 0)
                        requestParameter.Append($"Body:{request.GetBodyData()}\n");
                }
                catch
                {
                }

                if (requestParameter.ToString() != "")
                {
                    theEvent.Properties["requestParameter"] = $"RequestParameter：{requestParameter}";
                }
            }


            AsyncLogEventInfo asyncEventInfo = new AsyncLogEventInfo(theEvent, a => { });

            var requestTarget = LogManager.Configuration.AllTargets.Where(t => t is AsyncTargetWrapper && ((AsyncTargetWrapper)t).WrappedTarget.Name == "Requesttarget").SingleOrDefault();

            requestTarget.WriteAsyncLogEvent(asyncEventInfo);
        }
    }
}
