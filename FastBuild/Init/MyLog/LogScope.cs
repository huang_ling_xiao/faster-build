﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    /// <summary>
    /// 
    /// </summary>
    [Injection(DIPattern.Scoped)]
    public class LogScope
    {
        /// <summary>
        /// 
        /// </summary>
        public string LogScopeName { get; set; }
    }
}
