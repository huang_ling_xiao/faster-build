﻿using FastBuild.Init.JWT;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using NLog;
using NLog.Common;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Diagnostics;
using System.Reflection;

namespace FastBuild.Init.MyLog
{
    [Injection(DIPattern.Singleton)]
    internal class Log : ILog
    {
        private IHttpContextAccessor _httpContextAccessor { get; set; }

        public Log(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        //private Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Debug(string message, string fileName = "", params object[] args)
        {
            AsyncLogEventInfo theEvent = GetAsyncLogEventInfo(LogLevel.Debug, message, fileName, args: args);
            GetTarget("Debugtarget").WriteAsyncLogEvents(theEvent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Info(string message, string fileName = "", params object[] args)
        {
            AsyncLogEventInfo theEvent = GetAsyncLogEventInfo(LogLevel.Info, message, fileName, args: args);
            GetTarget("Infotarget").WriteAsyncLogEvents(theEvent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        public void Sql(string message, string fileName = "")
        {
            AsyncLogEventInfo theEvent = GetAsyncLogEventInfo(LogLevel.Debug, message, fileName);
            GetTarget("Sqltarget").WriteAsyncLogEvents(theEvent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName"></param>
        /// <param name="args"></param>
        public void Error(string message, string fileName = "", params object[] args)
        {
            AsyncLogEventInfo theEvent = GetAsyncLogEventInfo(LogLevel.Error, message, fileName, args: args);
            GetTarget("Errortarget").WriteAsyncLogEvents(theEvent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Error(Exception ex, string message, string fileName = "", params object[] args)
        {
            var request = _httpContextAccessor.HttpContext?.Request;
            AsyncLogEventInfo theEvent = GetAsyncLogEventInfo(LogLevel.Error, message, fileName, ex, args);

            if (request is not null)
            {

                if (_httpContextAccessor.HttpContext?.Items?.ContainsKey("RequestToken") == true)
                    theEvent.LogEvent.Properties["authorization"] = _httpContextAccessor.HttpContext?.Items["RequestToken"].ToString();

                StringBuilder requestParameter = new StringBuilder();

                if (request.QueryString.HasValue)
                    requestParameter.Append($"\nQuery:{request.QueryString.Value}");
                try
                {
                    if (request.Form is not null && request.Form.Count > 0)
                        requestParameter.Append($"\nForm:\n{string.Join("\n", request.Form.Select(form => $"Key:{form.Key},Value:{form.Value}").ToList())}");
                }
                catch
                {
                }

                try
                {
                    if (request.Body.Length > 0)
                        requestParameter.Append($"\nBody:{request.GetBodyData()}");
                }
                catch
                {
                }

                theEvent.LogEvent.Properties["requestParameter"] = requestParameter.ToString();
            }

            GetTarget("Errortarget").WriteAsyncLogEvents(theEvent);
        }

        private AsyncLogEventInfo GetAsyncLogEventInfo(LogLevel logLevel, string message, string fileName, Exception ex = null, params object[] args)
        {
            string controllerName = "", actionName = "";

            ControllerActionDescriptor controllerActionDescriptor = _httpContextAccessor.HttpContext?.GetEndpoint()?.Metadata?.GetMetadata<ControllerActionDescriptor>();

            if (controllerActionDescriptor is not null)
            {
                controllerName = controllerActionDescriptor.ControllerName;
                actionName = controllerActionDescriptor.ActionName;

                if (string.IsNullOrEmpty(fileName))
                    fileName = $"{controllerActionDescriptor.ControllerName}_{controllerActionDescriptor.ActionName}";
            }

            if (string.IsNullOrEmpty(fileName))
            {
                StackTrace stackTrace = new StackTrace(true);
                MethodBase methodBase = stackTrace.GetFrame(2).GetMethod();

                //异步
                if (methodBase.DeclaringType.DeclaringType is not null)
                    fileName = methodBase.DeclaringType.DeclaringType.Name;
                //同步
                else
                    fileName = methodBase.DeclaringType.Name;
            }

            //UniqueKey
            var uniqueKey = "";
            if (OptionSetting.LogUniqueKey is not null && _httpContextAccessor.HttpContext?.Items?.ContainsKey("RequestTokenString") == true)
            {
                try
                {
                    uniqueKey = OptionSetting.LogUniqueKey(_httpContextAccessor.HttpContext?.Items["RequestTokenString"].ToString());
                }
                catch { }
            }

            LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);

            if (!string.IsNullOrEmpty(actionName))
                actionName = "_" + actionName;
            if (!string.IsNullOrEmpty(uniqueKey))
                uniqueKey = "_" + uniqueKey;

            theEvent.Properties["fileName"] = fileName;
            theEvent.Properties["controllerName"] = controllerName;
            theEvent.Properties["actionName"] = actionName;
            theEvent.Properties["uniqueKey"] = uniqueKey;
            theEvent.Parameters = args;
            theEvent.Exception = ex;

            return new AsyncLogEventInfo(theEvent, a => { });
        }

        private Target GetTarget(string targetName)
        {
            return LogManager.Configuration.AllTargets.Where(t => t is AsyncTargetWrapper && ((AsyncTargetWrapper)t).WrappedTarget.Name == targetName).SingleOrDefault();
        }
    }
}
