﻿using NLog;
using NLog.Common;
using NLog.Targets.Wrappers;

namespace FastBuild.MyNLog
{
    internal static class GlobalLog
    {
        public static void LogGlobalException(Exception ex)
        {
            //Requesttarget
            LogEventInfo theEvent = new LogEventInfo(LogLevel.Error, "", "");

            theEvent.Properties["globalException"] = ex.ToString();

            //theEvent.Properties["globalException"] = @$"{ex}";

            AsyncLogEventInfo asyncEventInfo = new AsyncLogEventInfo(theEvent, a => { });

            var requestTarget = LogManager.Configuration.AllTargets.Where(t => t is AsyncTargetWrapper && ((AsyncTargetWrapper)t).WrappedTarget.Name == "GlobalExceptiontarget").SingleOrDefault();

            requestTarget.WriteAsyncLogEvent(asyncEventInfo);
        }

        public static void LogGlobalException(string message)
        {
            //Requesttarget
            LogEventInfo theEvent = new LogEventInfo(LogLevel.Error, "", "");

            theEvent.Properties["globalException"] = message;

            //theEvent.Properties["globalException"] = @$"{ex}";

            AsyncLogEventInfo asyncEventInfo = new AsyncLogEventInfo(theEvent, a => { });

            var requestTarget = LogManager.Configuration.AllTargets.Where(t => t is AsyncTargetWrapper && ((AsyncTargetWrapper)t).WrappedTarget.Name == "GlobalExceptiontarget").SingleOrDefault();

            requestTarget.WriteAsyncLogEvent(asyncEventInfo);
        }
    }
}
