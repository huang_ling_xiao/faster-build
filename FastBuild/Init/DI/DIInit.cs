﻿using System.Reflection;
using System.Linq;

namespace FastBuild.Init.DI
{
    internal static class DIInitClass
    {
        /// <summary>
        /// 初始化注入DI
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection DIInit(this IServiceCollection services)
        {
            List<AddDIModel> addDISource = new List<AddDIModel>();

            //给InitOption的SettingOption赋值
            var option = OptionSetting.DIInitOption;

            #region 处理各种类型的注入Type
            if (option is not null)
            {
                #region 单Type类型
                //命名空间注入Implementation
                foreach (var namespaceOne in option.NamespaceData)
                {
                    var typeList = AssemblyHelper.LoadTypeAssemblyByNamespace(namespaceOne.Namespace, namespaceOne.AssemblyName, isInterface: false);

                    addDISource.AddRange(
                            typeList.Select(s => new AddDIModel()
                            {
                                DIPattern = namespaceOne.DIPattern,
                                ServiceType = s
                            }));
                }

                //单个注入Implementation
                addDISource.AddRange(option.TypeData.Where(s => s.TypeList is not null)
                    .SelectMany(s => s.TypeList
                    .Select(t => new AddDIModel()
                    {
                        DIPattern = s.DIPattern,
                        ServiceType = t,
                    })));
                #endregion

                #region Interface类型
                //命名空间注入ServiceAndImplementation
                foreach (var item in option.ServiceAndImplementationNamespaceData)
                {
                    var instanceTypeList = AssemblyHelper.LoadTypeAssemblyByNamespace(item.Namespace, item.AssemblyName, isInterface: false);

                    var IServiceTypeList = AssemblyHelper.LoadTypeAssemblyByNamespace(item.IServiceNamespace, item.AssemblyName, isInterface: true);

                    foreach (var instanceType in instanceTypeList)
                    {
                        var interfaceName = item.Match.Replace("{Service}", instanceType.Name);

                        //替换完的名字相同 并且 该class必须继承这个interface
                        var IServiceData = IServiceTypeList
                            .Where(s => s.Name == interfaceName
                            && instanceType.GetInterface(interfaceName) is not null)
                            .SingleOrDefault();

                        if (IServiceData is not null)
                        {
                            addDISource.Add(new AddDIModel()
                            {
                                DIPattern = item.DIPattern,
                                ServiceType = instanceType,
                                IServiceType = IServiceData
                            });
                        }
                    }
                }

                //单个注入ServiceAndImplementation
                addDISource.AddRange(
                            option.ServiceAndImplementationTypeData
                            .Where(s => s.ServiceType.GetInterface(s.IServiceType.Name) is not null)
                            .Select(s => new AddDIModel()
                            {
                                DIPattern = s.DIPattern,
                                ServiceType = s.ServiceType,
                                IServiceType = s.IServiceType
                            }));
                #endregion
            }

            #region Attribute方式
            //InjectionAttribute特性注入
            var allAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(s => !s.FullName.StartsWith("System") && !s.FullName.StartsWith("Microsoft")).ToList();
            List<TypeInfo> list = new List<TypeInfo>();
            foreach (var assemblies in allAssemblies)
            {
                list.AddRange(assemblies.DefinedTypes.Where(s => s.CustomAttributes.Any(c => c.AttributeType == typeof(InjectionAttribute))).ToList());
            }

            foreach (var type in list)
            {
                var injectionAttribute = type.GetCustomAttribute<InjectionAttribute>();

                //如果选择注入的服务不为null，并且是Interface 但是却没有继承Interface就抛出异常
                if (injectionAttribute.IServeice is not null && injectionAttribute.IServeice.IsInterface && type.GetInterface(injectionAttribute.IServeice.Name) is null)
                    throw new UnableInjectionDependencyException($"{type.Name}未继承{injectionAttribute.IServeice.Name}，不能注入");

                var typeInterface = type.GetInterfaces().Where(t => !t.Assembly.FullName.StartsWith("System") && !t.Assembly.FullName.StartsWith("Microsoft")).ToList();
                addDISource.Add(new AddDIModel()
                {
                    DIPattern = injectionAttribute.DIPattern,
                    ServiceType = type,
                    IServiceType =
                    //如果没填写服务 但是却继承了Interface并且count还等于1就自动给该服务注入到Interface服务中
                    injectionAttribute.IServeice is null && typeInterface is not null && typeInterface.Count == 1
                    ? typeInterface.FirstOrDefault()
                    : (injectionAttribute.IServeice == type.AsType() ? null : injectionAttribute.IServeice)
                });
            }
            #endregion

            #endregion

            //begin AddDICommon
            addDISource.RemoveAll(s => s.ServiceType is null);
            addDISource = addDISource.DistinctBy(s => new { s.IServiceType, s.DIPattern, s.ServiceType }).ToList();

            var moreInterfaceTypeDISource = addDISource.DistinctBy(s => new { s.IServiceType, s.ServiceType }).Where(s => s.IServiceType is not null).GroupBy(s => s.IServiceType).ToList().Where(s => s.Count() > 1).ToDictionary(x => x.Key, x => x.ToList());

            if (moreInterfaceTypeDISource.Count >= 1)
                throw new UnableInjectionDependencyException($"Interface has more than one class {string.Join("", moreInterfaceTypeDISource.Select(s => $"\n{s.Key.Name},{string.Join("/", s.Value.Select(v => v.ServiceType.Name).ToList())}").ToList())}");

            var diInitNameList = services.AddDICommon(addDISource);

            if (diInitNameList.Count > 0)
                return services.ConsoleWriteLine($"Injection Init Success", diInitNameList);
            else
                return services;
        }
    }
}
