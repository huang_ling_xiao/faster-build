﻿namespace FastBuild
{
    public class DINamespaceAddModel
    {
        /// <summary>
        /// 命名空间
        /// </summary>
        public string Namespace { get; set; }

        /// <summary>
        /// 程序集名称 为空则使用DIInitOption的AssemblyName
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// 注入模式
        /// </summary>
        public DIPattern DIPattern { get; set; } = DIPattern.Singleton;
    }

    public class DITypeAddModel
    {
        /// <summary>
        /// 注入Type
        /// </summary>
        public List<Type> TypeList { get; set; }

        /// <summary>
        /// 注入模式
        /// </summary>
        public DIPattern DIPattern { get; set; } = DIPattern.Singleton;
    }

    public class DIServiceAndImplementationTypeAddModel
    {
        /// <summary>
        /// interfaceType
        /// </summary>
        public Type IServiceType { get; set; }

        /// <summary>
        /// 实现Type
        /// </summary>
        public Type ServiceType { get; set; }

        /// <summary>
        /// 注入模式
        /// </summary>
        public DIPattern DIPattern { get; set; } = DIPattern.Singleton;
    }

    public class DIServiceAndImplementationNamespaceAddModel : DINamespaceAddModel
    {
        /// <summary>
        /// Service命名空间
        /// </summary>
        public string IServiceNamespace { get; set; }

        /// <summary>
        /// Service和IService的规则匹配
        /// 必须包含Service的名字
        /// {Service} 替代符
        /// example：Service的名字是UserService，模板为：I{Service} 转过来就是IUserService 前后都可加字母
        /// </summary>
        public string Match { get; set; }
    }
}
