﻿namespace FastBuild.Init.DI
{
    public class DIInitOption
    {
        internal List<DINamespaceAddModel> NamespaceData { get; set; } = new List<DINamespaceAddModel>();

        internal List<DITypeAddModel> TypeData { get; set; } = new List<DITypeAddModel>();

        internal List<DIServiceAndImplementationTypeAddModel> ServiceAndImplementationTypeData { get; set; }
            = new List<DIServiceAndImplementationTypeAddModel>();

        internal List<DIServiceAndImplementationNamespaceAddModel> ServiceAndImplementationNamespaceData { get; set; }
        = new List<DIServiceAndImplementationNamespaceAddModel>();

        /// <summary>
        /// 根据Namespace注入
        /// </summary>
        /// <param name="serviceNamespace">Service的命名空间</param>
        /// <param name="dIPattern">注入类型</param>
        /// <param name="assemblyName">为空默认使用主程序集</param>
        public DIInitOption InjectionNamespace(string serviceNamespace, DIPattern dIPattern, string assemblyName = "")
        {
            NamespaceData.Add(new DINamespaceAddModel() { AssemblyName = assemblyName, DIPattern = dIPattern, Namespace = serviceNamespace });
            return this;
        }

        /// <summary>
        /// Type注入
        /// </summary>
        /// <param name="dIPattern">注入类型</param>
        /// <param name="typeList">Service的Type</param>
        /// <returns></returns>
        public DIInitOption InjectionType(DIPattern dIPattern, params Type[] typeList)
        {
            if (typeList == null || typeList.Length <= 0)
                throw new UnableInjectionDependencyException("typeList 不少于一个");
            TypeData.Add(new DITypeAddModel() { DIPattern = dIPattern, TypeList = typeList.ToList() });
            return this;
        }

        /// <summary>
        /// 根据Type注入IService和Service
        /// </summary>
        /// <param name="dIPattern">注入类型</param>
        /// <param name="serviceType">Service的Type</param>
        /// <param name="iServiceType">IService的Type</param>
        public DIInitOption InjectionServiceAndImplementationType(DIPattern dIPattern, Type serviceType, Type iServiceType)
        {
            ServiceAndImplementationTypeData.Add(new DIServiceAndImplementationTypeAddModel() { DIPattern = dIPattern, IServiceType = iServiceType, ServiceType = serviceType });
            return this;
        }

        /// <summary>
        /// 根据Namespace注入IService和Service
        /// </summary>
        /// <param name="serviceNamespace">Service的命名空间</param>
        /// <param name="iServiceNamespace">IService的命名空间</param>
        /// <param name="match">Service和IService的规则匹配，必须包含Service的名字，{Service}是Service名字替代符，example：Service的名字是UserService，match为：I{Service} 转过来就是IUserService 前后都可加字母</param>
        /// <param name="dIPattern">注入类型</param>
        /// <param name="assemblyName">为空默认使用主程序集</param>
        /// <returns></returns>
        public DIInitOption InjectionServiceAndImplementationNamespace(string serviceNamespace, string iServiceNamespace, string match, DIPattern dIPattern, string assemblyName = "")
        {
            ServiceAndImplementationNamespaceData.Add(new DIServiceAndImplementationNamespaceAddModel()
            {
                DIPattern = dIPattern,
                Namespace = serviceNamespace,
                IServiceNamespace = iServiceNamespace,
                AssemblyName = assemblyName,
                Match = match
            });
            return this;
        }
    }
}
