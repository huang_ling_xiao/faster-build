﻿using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace FastBuild.Init.Swagger
{
    /// <summary>
    /// 
    /// </summary>
    public class SwaggerInitOption
    {
        internal List<OpenApiParameter> _openApiParameterList { get; set; } = new List<OpenApiParameter>();

        /// <summary>
        /// 添加自定义全局参数
        /// </summary>
        /// <param name="parameter"></param>
        public void AddGlobalParameters(OpenApiParameter parameter)
        {
            _openApiParameterList.Add(parameter);
        }
    }
}
