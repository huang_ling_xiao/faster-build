﻿using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;

namespace FastBuild.Init.Setting
{
    /// <summary>
    /// Setting初始化
    /// </summary>
    internal static class SwaggerInitClass
    {
        /// <summary>
        /// Init Swagger
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection SwaggerInit(this IServiceCollection services, IConfiguration configuration)
        {
#if !DEBUG
            bool.TryParse(configuration["OpenSwagger"], out var openSwagger);

            if (!openSwagger) return services;
#endif
            var config = services.BuildServiceProvider().GetService<JwtConfig>();
            services.AddSwaggerGen(options =>
            {
                var entryAssembly = Assembly.GetEntryAssembly();

                options.SwaggerDoc("v1", new OpenApiInfo { Title = entryAssembly.GetName().Name, Version = "v1" });
                //注释
                var basePath = AppDomain.CurrentDomain.BaseDirectory;
                options.IncludeXmlComments(Path.Combine(basePath, entryAssembly.GetName().Name + ".xml"), true);

                // 开启oauth2安全描述
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "在下框中输入请求头中需要添加Jwt授权的Token",
                    Name = config.HeaderName,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                        {
                            new OpenApiSecurityScheme{
                                Reference = new OpenApiReference {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"}
                            },new string[] { }
                        }
                });

                //自定参数添加
                options.OperationFilter<AddParameters>();
            });

            return services;
        }

        public static WebApplication SwaggerInit(this WebApplication app, IConfiguration configuration)
        {
#if !DEBUG
            bool.TryParse(configuration["OpenSwagger"], out var openSwagger);

            if (!openSwagger) return app;
#endif

            var entryAssembly = Assembly.GetEntryAssembly();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", entryAssembly.GetName().Name);
            }).ConsoleWriteLine("Swagger Init Success");

            return app;
        }
    }

    internal class AddParameters : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (OptionSetting.SwaggerInitOption?._openApiParameterList is not null)
            {
                foreach (var item in OptionSetting.SwaggerInitOption._openApiParameterList)
                {
                    operation.Parameters.Add(item);
                }
            }
        }
    }
}
