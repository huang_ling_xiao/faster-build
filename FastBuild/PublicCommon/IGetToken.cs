﻿using Microsoft.AspNetCore.Http;

namespace FastBuild
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGetToken
    {
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <typeparam name="T">需要返回的类型</typeparam>
        /// <returns></returns>
        public T GetToken<T>();
    }

    /// <summary>
    /// 获取Token
    /// </summary>
    [Injection(DIPattern.Scoped)]
    public class MyGetToken : IGetToken
    {
        #region DI
        private readonly IJwtHelper _jwtHelper;

        private readonly IHttpContextAccessor _httpContextAccessor;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jwtHelper"></param>
        /// <param name="httpContextAccessor"></param>
        public MyGetToken(IJwtHelper jwtHelper, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _jwtHelper = jwtHelper;
        }

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="UnauthorizedException"></exception>
        public T GetToken<T>()
        {
            try
            {
                if (_httpContextAccessor?.HttpContext?.Items?.ContainsKey("RequestToken") == true)
                {
                    return _jwtHelper.Decrypt<T>(_httpContextAccessor?.HttpContext?.Items["RequestToken"].ToString());
                }

                throw new Exception();
            }
            catch
            {
                return default(T);
            }
        }
    }
}
