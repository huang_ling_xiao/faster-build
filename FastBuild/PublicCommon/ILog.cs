﻿namespace FastBuild
{
    /// <summary>
    /// 日志
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Debug
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        void Debug(string message, string fileName = "", params object[] args);

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        void Info(string message, string fileName = "", params object[] args);

        /// <summary>
        /// Sql
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        void Sql(string message, string fileName = "");

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        void Error(string message, string fileName = "", params object[] args);

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        void Error(Exception ex, string message, string fileName = "", params object[] args);
    }
}
