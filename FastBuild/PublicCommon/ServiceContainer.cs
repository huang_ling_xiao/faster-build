﻿using Microsoft.AspNetCore.Http;

namespace FastBuild
{
    /// <summary>
    /// 静态获取IOC服务
    /// </summary>
    public static class ServiceContainer
    {
        internal static IServiceProvider serviceProvider;

        /// <summary>
        /// 获取服务 
        /// Scope类型服务只在WebApi模式下能获取到同一个实例，非WebApi模式下，每次都是新的实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetService<T>() where T : class => (T)ReturnService(typeof(T));

        /// <summary>
        /// 获取服务 
        /// Scope类型服务只在WebApi模式下能获取到同一个实例，非WebApi模式下，每次都是新的实例
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetService(Type type) => ReturnService(type);

        /// <summary>
        /// 获取作用域ServiceProvider 非MVC模式下获取到是一个新的作用域
        /// </summary>
        /// <returns></returns>
        public static IServiceProvider GetScopeServiceProvider()
        {
            var httpContextAccessor = serviceProvider.GetService<IHttpContextAccessor>();
            if (httpContextAccessor != null && httpContextAccessor.HttpContext != null)
                return httpContextAccessor.HttpContext.RequestServices;

            return serviceProvider.CreateAsyncScope().ServiceProvider;
        }

        private static object ReturnService(Type type)
        {
            return GetScopeServiceProvider().GetServiceWithAutowired(type);
        }
    }
}
