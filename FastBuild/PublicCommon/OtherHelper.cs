﻿using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

namespace FastBuild
{
    /// <summary>
    /// 
    /// </summary>
    public static class OtherHelper
    {
        /// <summary>
        /// content中如果换行就加上\n 会自动进行对齐换行
        /// </summary>
        /// <param name="content"></param>
        /// <param name="level"></param>
        /// <param name="levelColor"></param>
        /// <param name="contentColor"></param>
        /// <param name="list"></param>
        public static void ConsoleWriteLine(string content, Level level, ConsoleColor levelColor = ConsoleColor.Green, ConsoleColor contentColor = ConsoleColor.White, List<string> list = null)
        {
            string levelString = $"{level}: ";

            Console.ForegroundColor = levelColor;
            Console.Write(levelString);

            Console.ForegroundColor = contentColor;
            Console.WriteLine(content.Replace("\n", $"\n{Regex.Replace(levelString, ".", " ")}"));

            Console.ResetColor();

            if (list is not null && list.Count > 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{Regex.Replace(levelString, ".", " ")}列表：");
                Console.ResetColor();
                foreach (var item in list)
                {
                    Console.WriteLine(Regex.Replace($"{Regex.Replace(levelString, ".", " ")}列表：", ".", " ") + item);
                }
            }
        }

		/// <summary>
		/// 获得body请求
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public static string GetBodyData(this HttpRequest request)
		{
			request.EnableBuffering();

			string body = "";
			var buffer = new MemoryStream();
			request.Body.Seek(0, SeekOrigin.Begin);
			request.Body.CopyTo(buffer);
			buffer.Position = 0;
			try
			{
				using (StreamReader streamReader = new StreamReader(buffer, Encoding.UTF8))
				{
					body = streamReader.ReadToEndAsync().Result;
				}
			}
			finally
			{
				if (buffer != null)
					buffer.Dispose();
			}
			return body;
		}
	}
}
