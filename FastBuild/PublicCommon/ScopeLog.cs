﻿namespace FastBuild
{
    /// <summary>
    /// 
    /// </summary>
    [Injection(DIPattern = DIPattern.Scoped)]
    public class ScopeLog
    {
        private readonly ILog _log;

        private readonly LogScope _scope;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="scope"></param>
        public ScopeLog(ILog log, LogScope scope)
        {
            _log = log;
            _scope = scope;
        }

        /// <summary>
        /// Debug
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Debug(string message, string fileName = "", params object[] args)
        {
            _log.Debug(message, string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : fileName, args);
        }

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Info(string message, string fileName = "", params object[] args)
        {
            _log.Info(message, string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : fileName, args);
        }

        /// <summary>
        /// Sql
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        public void Sql(string message, string fileName = "")
        {
            _log.Sql(message, string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : fileName);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Error(string message, string fileName = "", params object[] args)
        {
            _log.Error(message, string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : fileName, args);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="message"></param>
        /// <param name="fileName">日志名称</param>
        /// <param name="args"></param>
        public void Error(Exception ex, string message, string fileName = "", params object[] args)
        {
            _log.Error(ex, message, string.IsNullOrEmpty(fileName) ? _scope.LogScopeName : fileName, args);
        }
    }
}
