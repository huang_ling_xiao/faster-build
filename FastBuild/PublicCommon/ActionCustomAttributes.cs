﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Reflection;

namespace FastBuild
{
    /// <summary>
    /// 获取接口上的Attributes(单例中不能使用)
    /// </summary>
    [Injection(DIPattern.Scoped)]
    public class ActionCustomAttributes
    {
        public List<CustomAttributeData> CustomAttributeDatas { get; set; }

        public ActionCustomAttributes(IHttpContextAccessor httpContextAccessor)
        {
            ControllerActionDescriptor controllerActionDescriptor = httpContextAccessor.HttpContext?.GetEndpoint()?.Metadata?.GetMetadata<ControllerActionDescriptor>();

            IEnumerable<CustomAttributeData> enumerable = controllerActionDescriptor?.ControllerTypeInfo.GetCustomAttributesData();
            try
            {
                if (controllerActionDescriptor != null)
                {
                    enumerable = enumerable.Concat(controllerActionDescriptor?.MethodInfo.GetCustomAttributesData());
                }
            }
            catch
            {
            }

            CustomAttributeDatas = enumerable?.ToList();
        }
    }
}
