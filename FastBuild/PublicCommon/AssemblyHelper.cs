﻿using System.Reflection;

namespace FastBuild
{
    public class AssemblyHelper
    {
        /// <summary>
        /// 找到程序集中某个Namespace下的所有类型
        /// </summary>
        /// <param name="classNamespace">命名空间</param>
        /// <param name="assemblyName">程序集名称 默认查找主程序集</param>
        /// <param name="isInterface">是否Interface</param>
        /// <returns></returns>
        public static List<Type> LoadTypeAssemblyByNamespace(string classNamespace, string assemblyName = "", bool? isInterface = null)
        {
            Assembly mainAssemby = null;
            if (string.IsNullOrEmpty(assemblyName)) mainAssemby = Assembly.GetEntryAssembly();
            else mainAssemby = AppDomain.CurrentDomain.GetAssemblies().Where(s => s.GetName().Name == assemblyName).SingleOrDefault();

            classNamespace = classNamespace.Replace("{EntryProject}", mainAssemby.GetName().Name);

            //获取当前程序集的所有类
            return mainAssemby
                .GetTypes()
                //排除嵌套类型
                .Where(t => string.Equals(t.Namespace, classNamespace, StringComparison.Ordinal) && !t.IsNested)
                .Where(t => isInterface is not null ? t.IsInterface == isInterface : true)
                .ToList();
        }
    }
}
