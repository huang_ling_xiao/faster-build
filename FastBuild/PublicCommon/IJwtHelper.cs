﻿namespace FastBuild
{
    /// <summary>
    /// Jwt帮助类
    /// </summary>
    public interface IJwtHelper
    {
        /// <summary>
        /// 是否可以解密
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        bool DecryptOK(string token);

        /// <summary>
        /// 解密 失败的话会抛出异常
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="token"></param>
        /// <returns></returns>
        T Decrypt<T>(string token);

        /// <summary>
        /// Jwt加密
        /// </summary>
        /// <param name="body">加密内容</param>
        string Encrypt(object body);

        /// <summary>
        /// Jwt加密
        /// </summary>
        /// <param name="body">加密内容</param>
        /// <param name="second">过期时间</param>
        string Encrypt(object body, int second);

        /// <summary>
        /// 刷新Token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="lifetime"></param>
        /// <returns></returns>
        string RefreshToken(string token, int lifetime = 0);
    }
}
