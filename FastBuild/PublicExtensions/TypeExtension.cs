﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    public static class TypeExtension
    {
        /// <summary>
        /// Json字符串转Object
        /// </summary>
        /// <param name="type"></param>
        /// <param name="jsonString">Json字符串</param>
        /// <returns></returns>
        public static object jsonStringToObject(this Type type, string jsonString)
        {
            try
            {
                if (type.Name == "String" || jsonString is null)
                    return jsonString;
                else
                    return JsonConvert.DeserializeObject(jsonString, type);
            }
            catch { return jsonString; }
        }
    }
}
