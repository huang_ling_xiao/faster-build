﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastBuild
{
    /// <summary>
    /// 解析服务扩展类，从容器获取服务并解析其Autowired依赖。
    /// </summary>
    public static class ServiceProviderExtension
    {
        /// <summary>
        /// 解析指定实例的Autowired依赖
        /// </summary>
        /// <param name="instance">已创建的服务实例</param>
        /// <param name="serviceProvider">服务提供器</param>
        public static void WithAutowired(this object instance, IServiceProvider serviceProvider)
        {
            DependencyInjection.Resolve(serviceProvider, instance);
        }

        /// <summary>
        /// 解析指定实例的Autowired依赖
        /// </summary>
        /// <param name="instance">已创建的服务实例</param>
        /// <param name="serviceProvider">服务提供器</param>
        public static void WithAutowired<T>(this T instance, IServiceProvider serviceProvider)
        {
            DependencyInjection.Resolve(serviceProvider, instance);
        }

        /// <summary>
        /// 解析指定实例的Autowired依赖
        /// </summary>
        /// <param name="serviceProvider">服务提供器</param>
        /// <param name="type">服务类型，可以为IEnumerable&lt;&gt;</param>
        public static object GetServiceWithAutowired(this IServiceProvider serviceProvider, Type type)
        {
            object instance = serviceProvider.GetRequiredService(type);
            DependencyInjection.Resolve(serviceProvider, instance);
            return instance;
        }

        /// <summary>
        /// 从容器获取服务并解析Autowired依赖
        /// </summary>
        /// <typeparam name="T">服务类型，可以为IEnumerable&lt;&gt;</typeparam>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public static T GetServiceWithAutowired<T>(this IServiceProvider serviceProvider)
        {
            T instance = serviceProvider.GetRequiredService<T>();
            DependencyInjection.Resolve(serviceProvider, instance);
            return instance;
        }

        /// <summary>
        /// 添加后台服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="app"></param>
        public static void AddBackgroundService<T>(this WebApplication app) where T : class
        {
            T instance = app.Services.GetRequiredService<T>();
            DependencyInjection.Resolve(app.Services, instance);
        }
    }
}
