﻿namespace FastBuild
{
    public static class ConsoleExtensions
    {
        public static WebApplicationBuilder ConsoleWriteLine(this WebApplicationBuilder builder, string Content, List<string> list = null)
        {
            OtherHelper.ConsoleWriteLine(Content, Level.Info, ConsoleColor.Green,list: list);
            return builder;
        }

        public static WebApplication ConsoleWriteLine(this WebApplication app, string Content, List<string> list = null)
        {
            OtherHelper.ConsoleWriteLine(Content, Level.Info, ConsoleColor.Green, list: list);
            return app;
        }

        public static IApplicationBuilder ConsoleWriteLine(this IApplicationBuilder app, string Content, List<string> list = null)
        {
            OtherHelper.ConsoleWriteLine(Content, Level.Info, ConsoleColor.Green, list: list);
            return app;
        }

        public static IServiceCollection ConsoleWriteLine(this IServiceCollection service, string Content, List<string> list = null)
        {
            OtherHelper.ConsoleWriteLine(Content, Level.Info, ConsoleColor.Green, list: list);
            return service;
        }
    }
}
