﻿namespace FastBuild
{
    /// <summary>
    /// JsonSetting的Value来源
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValueFromAttribute : Attribute
    {
        /// <summary>
        /// Property Map
        /// </summary>
        public string Property { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ValueFromAttribute() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="property"></param>
        public ValueFromAttribute(string property)
        {
            Property = property;
        }
    }
}
