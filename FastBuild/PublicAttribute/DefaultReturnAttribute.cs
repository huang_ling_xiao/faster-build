﻿namespace FastBuild
{
    /// <summary>
    /// 默认返回Attribute，加在控制器或者方法上，用于不使用框架自带的全局返回
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DefaultReturnAttribute : Attribute
    {
        public DefaultReturnAttribute() { }
    }
}
