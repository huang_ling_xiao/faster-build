﻿namespace FastBuild
{
    /// <summary>
    /// 忽略特性 适用于SettingInit、DIInit中使用命名空间注入的时候忽略某个类
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectionIgnoreAttribute : Attribute
    {
    }
}
