﻿namespace FastBuild
{
    /// <summary>
    /// 注入特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class InjectionAttribute : Attribute
    {
        /// <summary>
        /// 注入模式
        /// </summary>
        public DIPattern DIPattern { get; set; }

        /// <summary>
        /// 如果是当前的继承的不需要赋值该字段也会自动注入Interface
        /// </summary>
        public Type IServeice { get; set; }

        /// <summary>
        /// 初始化方法 (无参数方法)
        /// </summary>
        public string InitMethod { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public InjectionAttribute() { }

        /// <summary>
        /// DI注入 特性
        /// </summary>
        /// <param name="DIPattern"></param>
        public InjectionAttribute(DIPattern DIPattern)
        {
            this.DIPattern = DIPattern;
        }

        /// <summary>
        /// DI注入 特性
        /// </summary>
        /// <param name="DIPattern"></param>
        /// <param name="IServeice">
        /// InterfaceType,如果当前类唯一继承了一个Interface,不需要传该项,
        /// 如果不想注入Interface,该项填写为当前类的Type/null</param>
        public InjectionAttribute(DIPattern DIPattern, Type IServeice)
        {
            this.DIPattern = DIPattern;
            this.IServeice = IServeice;
        }
    }
}
