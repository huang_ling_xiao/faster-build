﻿namespace FastBuild
{
    /// <summary>
    /// JsonSetting的数据来源
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class JsonFromAttribute : Attribute
    {
        /// <summary>
        /// Json文件路径
        /// </summary>
        public string Path { get; set; }

        public JsonFromAttribute() { }
    }
}
