﻿using FastBuild.Init.JWT;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace FastBuild.Init
{
    /// <summary>
    /// JwtMiddleware（Token认证）
    /// </summary>
    internal class JwtMiddleware : BaseMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly JwtConfig _jwtConfig;

        private readonly IJwtHelper _jwt;

        /// <summary>
        /// 管道执行到该中间件时候下一个中间件的RequestDelegate请求委托，如果有其它参数，也同样通过注入的方式获得
        /// </summary>
        /// <param name="next"></param>
        /// <param name="jwtConfig"></param>
        /// <param name="jwt"></param>
        public JwtMiddleware(RequestDelegate next, JwtConfig jwtConfig, IJwtHelper jwt)
        {
            //通过注入方式获得对象
            _next = next;
            _jwt = jwt;
            _jwtConfig = jwtConfig;
        }

        /// <summary>
        /// 自定义中间件要执行的逻辑
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            //不是接口调用 或者 没有开启JWT校验 或者 有AllowAnonymousAttribute
            if (!InvokeController(context)
                || !_jwtConfig.OpenGlobalAuthentication
                || actionDescriptor.EndpointMetadata.Any(s => s.GetType() == typeof(AllowAnonymousAttribute)))
            {
                await _next(context); return;
            }

            //获取token
            var token = context.Request.Headers.TryGetValue(_jwtConfig.HeaderName, out var value) ? value.First() : "";
            if (!_jwt.DecryptOK(token))
                throw new UnauthorizedException(_jwtConfig.ErrorMessage);

            context.Items.Add("RequestToken", token);
            context.Items.Add("RequestTokenString", _jwt.Decrypt<string>(token));

            await _next(context);

            //添加刷新token
            context.Response.Headers.Add(_jwtConfig.HeaderName, new[] { _jwt.RefreshToken(token) });
        }
    }
}
