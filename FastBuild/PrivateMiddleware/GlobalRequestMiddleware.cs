﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net;

namespace FastBuild.Init
{
    /// <summary>
    /// 全局请求MiddleWare（全局错误拦截，全局统一返回）
    /// </summary>
    internal class GlobalRequestMiddleware : BaseMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// 管道执行到该中间件时候下一个中间件的RequestDelegate请求委托，如果有其它参数，也同样通过注入的方式获得
        /// </summary>
        /// <param name="next"></param>
        public GlobalRequestMiddleware(RequestDelegate next)
        {
            //通过注入方式获得对象
            _next = next;
        }

        /// <summary>
        /// 自定义中间件要执行的逻辑
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            //不是接口调用
            if (!InvokeController(context))
            {
                await _next(context); return;
            }

            var oldBody = context.Response.Body;
            try
            {
                var request = context.Request;

                #region 兼容Query中参数名字为xxx[]无法对应到实体
                if (request.QueryString.Value.Contains("[]"))
                {
                    var requestQuery = request.QueryString.Value.Substring(1, request.QueryString.Value.Length - 1).Split("&");

                    for (int i = 0; i < requestQuery.Length; i++)
                    {
                        var item = requestQuery[i];
                        var fileName = item.Substring(0, item.IndexOf('='));
                        var newFileName = fileName.Replace("[]", "");
                        requestQuery[i] = item.Replace(fileName, newFileName);
                    }
                    request.QueryString = new QueryString($"?{string.Join("&", requestQuery)}");
                }
                #endregion

                #region 全局返回实现逻辑
                using (var ms = new MemoryStream())
                {
                    context.Response.Body = ms;
                    await _next(context);

                    //重定向问题
                    if (context.Response.StatusCode == 302) return;

                    context.Response.Body = oldBody;

                    ms.Seek(0, SeekOrigin.Begin);

                    var returnType = actionDescriptor.MethodInfo.ReturnType;

                    bool isVoid = returnType.Name == "Void" || returnType.Name == "Task";

                    //task 取item
                    if (returnType.Name == "Task`1")
                        returnType = actionDescriptor.MethodInfo.ReturnType.GetProperty("Result").PropertyType;

                    var actionTypeFullNames = new List<string>() { "Microsoft.AspNetCore.Mvc.IActionResult", "Microsoft.AspNetCore.Mvc.ActionResult" };

                    //actionResult || 有默认返回Attribute 不进行全局返回
                    if (actionTypeFullNames.Contains(returnType.FullName) || actionDescriptor.EndpointMetadata.Where(s => s.GetType() == typeof(DefaultReturnAttribute)).Any())
                    {
                        byte[] data = ms.ToArray();

                        await context.Response.Body.WriteAsync(data);

                        return;
                    }

                    using (var sr = new StreamReader(ms))
                    {
                        var response = await sr.ReadToEndAsync();
                        await WriteRespone(context.Response,
                            isVoid ? null : returnType.jsonStringToObject(response),
                            (int)HttpStatusCode.OK,
                            oldBody,
                            isDefaultReturn: actionDescriptor.EndpointMetadata.Where(s => s.GetType() == typeof(DefaultReturnAttribute)).Any());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                switch (ex)
                {
                    case OutPutException:
                        await WriteRespone(context.Response, ex.Message, (int)HttpStatusCode.InternalServerError, oldBody, isError: true, returnDataCode: 450);
                        break;
                    case UnauthorizedException:
                        await WriteRespone(context.Response, ex.Message, (int)HttpStatusCode.Unauthorized, oldBody, isError: true);
                        break;
                    default:
                        await WriteRespone(context.Response, ex.Message, (int)HttpStatusCode.InternalServerError, oldBody, isError: true);
                        throw;
                }
            }
        }
        private async Task WriteRespone(HttpResponse response, object returnData, int statusCode, Stream oldBody, bool isError = false, int returnDataCode = 0, bool isDefaultReturn = false)
        {
            response.Body = oldBody;
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);

            if (isDefaultReturn)
                sw.Write(returnData);
            else
            {
                sw.Write(JsonConvert.SerializeObject(new BaseReturnModel()
                {
                    Code = returnDataCode == 0 ? statusCode : returnDataCode,
                    Error = isError ? returnData : null,
                    Data = isError ? null : returnData
                }, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }));
                response.ContentType = "application/json;charset=utf-8";
            }

            sw.Flush(); sw.Close(); sw.Dispose();

            byte[] bytes = ms.ToArray();
            ms.Close(); ms.Dispose();

            response.ContentLength = bytes.Length;

            response.StatusCode = statusCode;

            await response.Body.WriteAsync(bytes);
        }
    }
}
