﻿using FastBuild.MyNLog;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace FastBuild.Init
{
    /// <summary>
    /// RequestLogMiddleware（请求日志）
    /// </summary>
    internal class RequestLogMiddleware : BaseMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly RequestLog _log;

        private Stopwatch stopwatch = null;

        /// <summary>
        /// 管道执行到该中间件时候下一个中间件的RequestDelegate请求委托，如果有其它参数，也同样通过注入的方式获得
        /// </summary>
        /// <param name="next"></param>
        /// <param name="log"></param>
        public RequestLogMiddleware(RequestDelegate next, RequestLog log)
        {
            //通过注入方式获得对象
            _next = next;
            _log = log;
        }

        /// <summary>
        /// 自定义中间件要执行的逻辑
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            //开启接口时间记录
            stopwatch = Stopwatch.StartNew();

            //不是接口调用
            if (!InvokeController(context))
            {
                await _next(context); return;
            }

            try
            {
                //开启body倒带功能 不然body只能被读取一次
                context.Request.EnableBuffering();

                await _next(context);
            }
            catch (Exception ex)
            {
                GlobalLog.LogGlobalException(ex);
            }
            finally
            {
                stopwatch.Stop();
                _log.LogRequest(stopwatch.Elapsed.TotalSeconds);
            }
        }
    }
}
