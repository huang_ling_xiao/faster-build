﻿using FastBuild.Init.JWT;
using FastBuild.MyNLog;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;

namespace FastBuild.Init
{
    /// <summary>
    /// BaseMiddleware
    /// </summary>
    internal class BaseMiddleware
    {
        public ControllerActionDescriptor actionDescriptor;

        public bool InvokeController(HttpContext context)
        {
            actionDescriptor = context.GetEndpoint()?.Metadata?.GetMetadata<ControllerActionDescriptor>();

            return actionDescriptor is not null;
        }
    }
}
