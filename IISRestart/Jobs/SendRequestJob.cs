﻿using FastBuild;
using FastBuild.TimeJob;

namespace Wechat_PublicNumber.Jobs
{
    public class SendRequestJob : IJob
    {
        private ILog _logger;
        public SendRequestJob(ILog logger)
        {
            _logger = logger;
        }

        public async Task Execute()
        {
            var httpclient = new HttpClient();
            var clientReturn = httpclient.GetAsync("http://101.42.227.212/WeChat/IISRestart").Result;
            httpclient.Dispose();
            clientReturn.Dispose();
            Console.WriteLine($"SendRequestJob Execute OK", "SendRequestJob");
            _logger.Info($"SendRequestJob Execute OK", "SendRequestJob");
            GC.Collect();
        }
    }
}
