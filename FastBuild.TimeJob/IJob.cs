﻿namespace FastBuild.TimeJob
{
    /// <summary>
    /// Job定义
    /// </summary>
    public interface IJob
    {
        /// <summary>
        /// 执行任务
        /// </summary>
        /// <returns></returns>
        Task Execute();
    }
}
