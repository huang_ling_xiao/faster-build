﻿//namespace FastBuild.TimeJob
//{
//    /// <summary>
//    /// JobFilter
//    /// 提供执行前和执行后用户业务处理
//    /// </summary>
//    public interface IJobFilter
//    {
//        /// <summary>
//        /// Job执行前
//        /// </summary>
//        /// <returns></returns>
//        Task OnJobExecuting();

//        /// <summary>
//        /// Job执行后
//        /// </summary>
//        /// <returns></returns>
//        Task OnJobExecuted();
//    }

//    /// <summary>
//    /// 回调返回类
//    /// </summary>
//    public class JobResultModel
//    {
//        /// <summary>
//        /// 执行结果
//        /// </summary>
//        public JobStatus Sutus { get; set; }

//        /// <summary>
//        /// 什么时候执行的
//        /// </summary>
//        public DateTime ExecuteTime { get; set; }

//        /// <summary>
//        /// 异常
//        /// </summary>
//        public Exception Exception { get; set; }
//    }
//}
