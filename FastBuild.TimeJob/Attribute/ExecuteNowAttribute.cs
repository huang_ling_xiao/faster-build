﻿namespace FastBuild.TimeJob
{
    /// <summary>
    /// 立即执行
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ExecuteNowAttribute : Attribute
    {
    }
}
