﻿namespace FastBuild.TimeJob
{
    /// <summary>
    /// 运行中跳过本次
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class SkipWhileExecutingAttribute : Attribute
    {
    }
}
