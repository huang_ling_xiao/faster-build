﻿namespace FastBuild.Init.TimeJob
{
    public class TimeJobInitOption
    {
        /// <summary>
        /// 任务setting的文件地址
        /// </summary>
        public string JobSettingPath { get; set; } = "Setting/TimedJobSetting.json";

        /// <summary>
        /// 任务执行的最大错误次数
        /// </summary>
        public int MaxErrorCount { get; set; } = 5;

        /// <summary>
        /// 错误回调函数
        /// IServiceProvider: 服务提供者
        /// string: 任务名称
        /// int: 当前错误次数
        /// bool: 任务是否停止
        /// </summary>
        public Func<IServiceProvider, string, int, bool, Task> ErrorHandler { get; set; }
    }
}
